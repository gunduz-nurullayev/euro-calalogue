from django.conf.urls import patterns, url
from company_account import views
# from workspace import views as wp_views


urlpatterns = patterns('',

   url(r'^registration/$',views.partner_register,  name='registration'),
    url(r'^login/$',   views.user_login,  name='user_login'),
    url(r'^profile/update/$',   views.profile_update,  name='profile_update'),
    # url(r'^profile/$',   views.profile_info,  name='profile_info'),
    url(r'^logout/$',   views.user_logout,  name='user_logout'),
    url(r'^password/change/$',   views.userPasswordChange,  name='password_change'),

    url(r'^edit/$',   views.companyChange,  name='edit_company'),

    url(r'^company-profile/edit/$',   views.companyProfileChange,  name='edit_profile'),
    url(r'^company-capacity/edit/$',   views.companyCapacityChange,  name='edit_capacity'),

    url(r'^company-clients/new/$',   views.add_client,  name='add_client'),
    url(r'^company-clients/(?P<id>.*?)/$',   views.companyClientChange,  name='edit_client'),

    url(r'^company-products/new/$',   views.add_product,  name='add_product'),
    url(r'^company-products/(?P<id>.*?)/$',   views.companyProductChange,  name='edit_product'),

    url(r'^company-projects/new/$',   views.add_project,  name='add_project'),
    url(r'^company-projects/(?P<id>.*?)/$',   views.companyProjectChange,  name='edit_project'),

    url(r'^company-partners/new/$',   views.add_partner,  name='add_partner'),
    url(r'^company-partners/(?P<id>.*?)/$',   views.companyPartnerChange,  name='edit_partner'),

    url(r'^company-employees/new/$',   views.add_employee,  name='add_employee'),
    url(r'^company-employees/(?P<id>.*?)/$',   views.companyEmplooyesChange,  name='edit_employee'),

    url(r'^company-partnerships/new/$',   views.add_partnership,  name='add_partnership'),
    url(r'^company-partnerships/(?P<id>.*?)/$',   views.companyPartnershipChange,  name='edit_partnership'),

    url(r'^company-certificates/new/$',   views.add_certificate,  name='add_certificate'),
    url(r'^company-certificates/(?P<id>.*?)/$',   views.companyCertificateChange,  name='edit_certificate'),

    url(r'^company-technologies/new/$',   views.add_technology,  name='add_technology'),
    url(r'^company-technologies/(?P<id>.*?)/$',   views.companyTechnologyChange,  name='edit_technology'),

    url(r'^$', views.company_page,  name='company_page'),






    url(r'^confirm/(?P<activation_key>[-\w]+)/$',   views.user_confirm,  name='user_confirm'),
    #user actions

)