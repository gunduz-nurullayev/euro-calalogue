from django.views import generic as generic_views
# from django.views import FormMixin, ListView
# from . import models,settings
# from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext
# Http
from django.http import HttpResponse, Http404,HttpResponseRedirect
#json
from django.core.serializers.json import DjangoJSONEncoder
import json
#mail
from django.core.mail import send_mail
#settings
from eurocatalog.settings import EMAIL_HOST_USER, PORTAL_URL
# models
#form
from company_account.forms import RegisterForm,LoginForm,PasswordChangeForm,CustomUserCreationForm,UpdatePartnerForm,\
                                UpdateUserForm
from django.shortcuts import render,render_to_response,get_object_or_404,redirect
# Create your views here.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BOOK CATEGORY LIST~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from django.http import HttpResponseRedirect,Http404
# from .forms import UploadFileForm
from django.utils import translation
from django.utils import timezone
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core.context_processors import csrf
from django.core.mail import send_mail, BadHeaderError
from django.db.models import Q
# Create your views here.
# from core.models import User
from django.views.generic.edit import FormMixin
from django.views.generic.list import ListView

from news.models import News
from publications.models import Publications
from sites.models import Sites
from exhibitions.models import Exhibitions
from core.models import Company,CompanyProfile,CompanyProducts,CompanyProjects,CompanyPartnership,CompanyCapacity,Categories,\
    CompanyPartners,CompanyClients,CompanyEmployees,UserProfile,UserPasswordRecovery,CustomUserManager,User,CompanyCertificates,CompanyTechnology
from business_info.models import Stage,BusinessInfo
#python
import datetime, random, sha
# Authentication
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
#Forms
from core.forms import CompanyPartnersForm,CompanyProjectsForm,CompanyProductsForm,CompanyClientsForm,CompanyEmployeesForm,\
    CompanyCertificatesForm,CompanyPartnershipForm,CompanyTechnologyForm,CompanyProfileForm,CompanyCapacityForm,CompanyForm


# Create your views here.
# class CompanyPageView(ListView):
#     model = Company
#     template_name = 'company_account/company_page.html'
#     user = request.user
#
#     @login_required()
#
#     def get_context_data(self, **kwargs):
#         context = super(CompanyPageView, self).get_context_data(**kwargs)
#         # user = self.request.user
#         # context['user'] = user
#
#         return context

@login_required()
def company_page(request):
    company = Company.objects.get(id=request.user.company_id)
    company_profile = CompanyProfile.objects.get(company=company.id)
    company_capacity = CompanyCapacity.objects.get(company=company.id)

    company_projects = CompanyProjects.objects.filter(company = company.id)
    company_products = CompanyProducts.objects.filter(company=company.id)
    company_partners = CompanyPartners.objects.filter(company=company.id)
    company_partnership = CompanyPartnership.objects.filter(company=company.id)
    company_employees = CompanyEmployees.objects.filter(company=company.id)
    company_clients = CompanyClients.objects.filter(company=company.id)
    company_certificates = CompanyCertificates.objects.filter(company=company.id)
    company_technology = CompanyTechnology.objects.filter(company=company.id)

    return render_to_response('company_account/company_page.html',
                              {'company':company,'company_projects':company_projects,
                               'company_products':company_products,'company_partners':company_partners,
                               'company_partnership':company_partnership,'company_employees':company_employees,
                              'company_clients':company_clients,'company_certificates':company_certificates,
                              'company_technology':company_technology,
                              'company_profile':company_profile,
                              'company_capacity':company_capacity
                              },
                              context_instance=RequestContext(request))

def add_partner(request):

    form = CompanyPartnersForm()

    if request.method == 'POST':
        form = CompanyPartnersForm(request.POST, request.FILES)

        if form.is_valid():
            partner = form.save(commit=False)
            partner.save()

        return redirect('/account')
    return render_to_response('company_account/add/add_partner.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )

def add_project(request):

    form = CompanyProjectsForm()

    if request.method == 'POST':
        form = CompanyProjectsForm(request.POST, request.FILES)
        if form.is_valid():
            project = form.save(commit=False)
            project.save()

        return redirect('/account')
    return render_to_response('company_account/add/add_project.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )

def add_product(request):

    form = CompanyProductsForm()

    if request.method == 'POST':
        form = CompanyProductsForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save(commit=False)
            product.save()

        return redirect('/account')
    return render_to_response('company_account/add/add_product.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )

def add_client(request):

    form = CompanyClientsForm()

    if request.method == 'POST':
        form = CompanyClientsForm(request.POST, request.FILES)
        if form.is_valid():
            client = form.save(commit=False)
            client.save()

        return redirect('/account')
    return render_to_response('company_account/add/add_client.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )


def add_employee(request):

    form = CompanyEmployeesForm()
    if request.method == 'POST':
        form = CompanyEmployeesForm(request.POST, request.FILES)
        # when form is valid register user
        if form.is_valid():
            employee = form.save(commit=False)
            employee.save()

        return redirect('/account')
    return render_to_response('company_account/add/add_employee.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )

def add_partnership(request):

    form = CompanyPartnershipForm()

    if request.method == 'POST':
        form = CompanyPartnershipForm(request.POST, request.FILES)
        # when form is valid register user
        if form.is_valid():
            partnership = form.save(commit=False)
            partnership.save()
        return redirect('/account')
    return render_to_response('company_account/add/add_partnership.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )


def add_certificate(request):
    form = CompanyCertificatesForm()

    if request.method == 'POST':
        form = CompanyCertificatesForm(request.POST, request.FILES)
        # when form is valid register user
        if form.is_valid():
            certificate = form.save(commit=False)
            certificate.save()

        return redirect('/account')
    return render_to_response('company_account/add/add_certificate.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )

def add_technology(request):

    form = CompanyTechnologyForm()

    if request.method == 'POST':
        form = CompanyTechnologyForm(request.POST)
        # when form is valid register user
        if form.is_valid():
            technology = form.save(commit=False)
            technology.save()

        return redirect('/account')
    return render_to_response('company_account/add/add_technology.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )

@login_required
def companyPartnerChange(request,id):
    partner = CompanyPartners.objects.get(pk=id)
    user = request.user

    partnerForm = CompanyPartnersForm(request.POST,request.FILES, instance=partner)

    if request.method == 'POST':
        if partnerForm.is_valid():
            update_partner = partnerForm.save(commit=False)
            update_partner.save()
            return redirect('/account')

    else:
        partnerForm = CompanyPartnersForm(instance=partner)

    return render_to_response('company_account/edit/edit_partner.html',
                                 {'partnerForm': partnerForm,'partnerid':id},
                                 context_instance=RequestContext(request)
                              )

@login_required
def companyProductChange(request,id):
    product = CompanyProducts.objects.get(pk=id)
    user = request.user

    productForm = CompanyProductsForm(request.POST,request.FILES, instance=product)

    if request.method == 'POST':
        if productForm.is_valid():
            update_product = productForm.save(commit=False)
            update_product.save()
            return redirect('/account')

    else:
        productForm = CompanyProductsForm(instance=product)

    return render_to_response('company_account/edit/edit_product.html',
                                 {'productForm': productForm,'productid':id},
                                 context_instance=RequestContext(request)
                              )

@login_required
def companyProjectChange(request,id):
    project = CompanyProjects.objects.get(pk=id)
    user = request.user

    projectForm = CompanyProjectsForm(request.POST,request.FILES, instance=project)

    if request.method == 'POST':
        if projectForm.is_valid():
            update_project = projectForm.save(commit=False)
            update_project.save()
            return redirect('/account')

    else:
        projectForm = CompanyProjectsForm(instance=project)

    return render_to_response('company_account/edit/edit_project.html',
                                 {'projectForm': projectForm,'projectid':id},
                                 context_instance=RequestContext(request)
                              )

@login_required
def companyPartnershipChange(request,id):
    partnership = CompanyPartnership.objects.get(pk=id)
    user = request.user

    partnershipForm = CompanyPartnershipForm(request.POST,request.FILES, instance=partnership)

    if request.method == 'POST':
        if partnershipForm.is_valid():
            update_partnership = partnershipForm.save(commit=False)
            update_partnership.save()
            return redirect('/account')

    else:
        partnershipForm = CompanyPartnershipForm(instance=partnership)

    return render_to_response('company_account/edit/edit_partnership.html',
                                 {'partnershipForm': partnershipForm,'partnershipid':id},
                                 context_instance=RequestContext(request)
                              )

@login_required
def companyClientChange(request,id):
    client = CompanyClients.objects.get(pk=id)
    user = request.user

    clientForm = CompanyClientsForm(request.POST,request.FILES, instance=client)

    if request.method == 'POST':
        if clientForm.is_valid():
            update_client = clientForm.save(commit=False)
            update_client.save()
            return redirect('/account')

    else:
        clientForm = CompanyClientsForm(instance=client)

    return render_to_response('company_account/edit/edit_client.html',
                                 {'clientForm': clientForm,'clientid':id},
                                 context_instance=RequestContext(request)
                              )
@login_required
def companyCertificateChange(request,id):
    certificate = CompanyCertificates.objects.get(pk=id)
    user = request.user

    certificateForm = CompanyCertificatesForm(request.POST,request.FILES, instance=certificate)

    if request.method == 'POST':
        if certificateForm.is_valid():
            update_certificate = certificateForm.save(commit=False)
            update_certificate.save()
            return redirect('/account')

    else:
        certificateForm = CompanyCertificatesForm(instance=certificate)

    return render_to_response('company_account/edit/edit_certificate.html',
                                 {'certificateForm': certificateForm,'certificateid':id},
                                 context_instance=RequestContext(request)
                              )


@login_required
def companyTechnologyChange(request,id):
    technology = CompanyTechnology.objects.get(pk=id)
    user = request.user

    technologyForm = CompanyTechnologyForm(request.POST,request.FILES, instance=technology)

    if request.method == 'POST':
        if technologyForm.is_valid():
            update_technology = technologyForm.save(commit=False)
            update_technology.save()
            return redirect('/account')

    else:
        technologyForm = CompanyTechnologyForm(instance=technology)

    return render_to_response('company_account/edit/edit_technology.html',
                                 {'technologyForm': technologyForm,'technologyid':id},
                                 context_instance=RequestContext(request)
                              )

@login_required
def companyEmplooyesChange(request,id):
    employee = CompanyEmployees.objects.get(pk=id)
    user = request.user

    employeeForm = CompanyEmployeesForm(request.POST,request.FILES, instance=employee)

    if request.method == 'POST':
        if employeeForm.is_valid():
            update_employee = employeeForm.save(commit=False)
            update_employee.save()
            return redirect('/account')

    else:
        employeeForm = CompanyEmployeesForm(instance=employee)

    return render_to_response('company_account/edit/edit_employee.html',
                                 {'employeeForm': employeeForm,'employeeid':id},
                                 context_instance=RequestContext(request)
                              )
@login_required
def companyProfileChange(request):
    profile = CompanyProfile.objects.get(company_id=request.user.company_id)
    user = request.user

    profileForm = CompanyProfileForm(request.POST,request.FILES, instance=profile)

    if request.method == 'POST':
        if profileForm.is_valid():
            update_profile = profileForm.save(commit=False)
            update_profile.save()
            return redirect('/account')

    else:
        profileForm = CompanyProfileForm(instance=profile)

    return render_to_response('company_account/edit/edit_profile.html',
                                 {'profileForm': profileForm},
                                 context_instance=RequestContext(request)
                              )
@login_required
def companyCapacityChange(request):
    capacity = CompanyCapacity.objects.get(company_id=request.user.company_id)
    user = request.user

    capacityForm = CompanyCapacityForm(request.POST,request.FILES, instance=capacity)

    if request.method == 'POST':
        if capacityForm.is_valid():
            update_capacity = capacityForm.save(commit=False)
            update_capacity.save()
            return redirect('/account')

    else:
        capacityForm = CompanyCapacityForm(instance=capacity)

    return render_to_response('company_account/edit/edit_capacity.html',
                                 {'capacityForm': capacityForm},
                                 context_instance=RequestContext(request)
                              )

@login_required
def companyChange(request):
    company = Company.objects.get(id=request.user.company_id)
    # category = Categories.objects.filter(company=request.user.company_id)
    user = request.user
    companyForm = CompanyForm(request.POST,request.FILES, instance=company,)

    if request.method == 'POST':
        # return HttpResponse(companyForm.errors)
        if companyForm.is_valid():
            update_capacity = companyForm.save(commit=False)
            # update_capacity.category = company.category
            update_capacity.last_check_date = datetime.datetime.today()
            update_capacity.save()
            companyForm.save_m2m()
            return redirect('/account')

    else:
        companyForm = CompanyForm(instance=company)

    return render_to_response('company_account/edit/edit_company.html',
                                 {'companyForm': companyForm,},
                                 context_instance=RequestContext(request)
                              )

def partner_register(request):
    form = RegisterForm()
    if request.method == 'POST':
        form = RegisterForm(request.POST, request.FILES)
        user = User()
        # when form is valid register user
        # return HttpResponse(form.errors)

        if form.is_valid():
            company = form.save()

            #save user
            user.email = form.cleaned_data["user_email"]
            # user.name = form.cleaned_data["name"]
            user.company = company
            # user.location = company.location
            # user.city = company.city
            # user.manager = True
            # set password for user and set deactive user
            user.set_password(form.cleaned_data["password1"])
            user.is_active = False
            user.save()
            salt = sha.new(str(random.random())).hexdigest()[:5]
            activation_key = sha.new(salt+company.name).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            # Create and save their profile
            new_profile = UserProfile(user=user,
                                      activation_key=activation_key,
                                      key_expires=key_expires)
            new_profile.save()
            company_profile = CompanyProfile(
                        company = user.company,
                        mission = '',
                        history = '',

            )
            company_profile.save()

            company_capacity = CompanyCapacity(
                company =user.company,
                manufempl ='',
                manageempl ='',
                volume = '',
                production_space ='',
            )
            company_capacity.save()

            confirm_url = redirect('company_account:user_confirm', new_profile.activation_key)['Location']
            # Send an email with the confirmation link
            email_subject = 'Your new partnership account confirmation'
            email_body = "\tHello, %s, and thanks for signing up for an account!\n\n \
            To activate your account, click this link within 48  hours:\n\n %s%s" % (
                                                                                    company.name,
                                                                                    PORTAL_URL,
                                                                                    confirm_url[1:]) #todo confirm url
            send_mail(email_subject,
                      email_body,
                      EMAIL_HOST_USER,
                      [user.email])
            form = RegisterForm()
            # return HttpResponse(form.errors)

            return render_to_response('company_account/register.html',
                                      {'message_sent': True},
                                      context_instance=RequestContext(request))

    return render_to_response('company_account/register.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )


#Confirm user registration with email
def user_confirm(request, activation_key):
    user_profile = get_object_or_404(UserProfile,
                                     activation_key=activation_key)
    key_expires = None
    if user_profile.user.is_active:
        return render_to_response('company_account/confirm.html', {'has_account': True}, context_instance=RequestContext(request))

    key_expires = user_profile.key_expires.replace(tzinfo=None)
    if key_expires < datetime.datetime.today():
        return render_to_response('company_account/confirm.html', {'expired': True})
    user_account = user_profile.user

    user_account.is_active = True
    user_account.save()
    return render_to_response('company_account/confirm.html', {'success': True,'user':user_account}, context_instance=RequestContext(request))



#profile view for login user
@login_required
def profile_info(request):
    user = request.user
    partner = user.company
    return render_to_response('company_account/profile_info.html',
                                  {'user':user, 'partner':partner},
                                  context_instance=RequestContext(request)
                              )


def user_login(request):
    req_url=str(request.GET.get('next',''))
    # sticks in a POST or renders empty form
    form = LoginForm(request.POST or None)
    # when form is valid login user
    if form.is_valid():
        next = request.POST.get('next',False)
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        user = authenticate(username=username, password=password)
        login(request, user)
        # Redirect to a success page.
        if next:
            return redirect(next)
        return redirect('index')
    # else:
    #     return redirect('/')

    return render_to_response('base.html',
                                  {'form': form, 'req_url':req_url},
                                  context_instance=RequestContext(request)
                              )

#update profile information
@login_required
def profile_update(request):
    user = request.user
    partner = user.partner

    userForm =None
    partnerForm =None

    if request.method == 'POST':
        partnerForm = UpdatePartnerForm(request.POST,request.FILES, instance=partner)
        userForm = UpdateUserForm(request.POST, instance=user)
        if userForm.is_valid() and not partnerForm.is_valid() and not request.user.manager:
            update_user = userForm.save()
            update_user.phone = userForm.cleaned_data['user_phone']
            update_user.save()
            return redirect('company_account:profile_info')
        elif partnerForm.is_valid() and userForm.is_valid():
            update_partner = partnerForm.save()
            update_user = userForm.save()
            update_user.phone = userForm.cleaned_data['user_phone']
            update_user.country = update_partner.country
            update_user.city = update_partner.city
            update_user.save()
            return redirect('company_account:profile_info')

    else:
        partnerForm = UpdatePartnerForm(instance=partner)
        userForm = UpdateUserForm(instance=user, initial={'user_phone':user.phone})



    return render_to_response('company_account/profile_update.html',
                                 {'partnerForm': partnerForm, 'userForm':userForm},
                                 context_instance=RequestContext(request)
                              )


# logout current user
@login_required
def user_logout(request):
    logout(request)
    return redirect('index')

#change user password
@login_required
def userPasswordChange(request):
    user = request.user
    # sticks in a POST or renders empty form
    #passing user and data varriable PasswordChangeForm
    form = PasswordChangeForm(user = request.user, data = request.POST or None)
    # when form is valid change password
    if form.is_valid():
        user.set_password(form.cleaned_data["password1"])
        user.save()
        return redirect('company_account:profile_info')

    return render_to_response('company_account/change_password.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )
