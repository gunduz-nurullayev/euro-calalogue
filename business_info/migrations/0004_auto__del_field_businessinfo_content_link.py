# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'BusinessInfo.content_link'
        db.delete_column(u'business_info_businessinfo', 'content_link_id')


    def backwards(self, orm):
        # Adding field 'BusinessInfo.content_link'
        db.add_column(u'business_info_businessinfo', 'content_link',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=2, related_name='businessinfo_content', to=orm['contenttypes.ContentType']),
                      keep_default=False)


    models = {
        u'business_info.businessinfo': {
            'Meta': {'object_name': 'BusinessInfo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Locations']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'business_info.stage': {
            'Meta': {'object_name': 'Stage'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'core.locations': {
            'Meta': {'object_name': 'Locations'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['business_info']