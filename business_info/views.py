from django.shortcuts import render
# Http
from django.http import HttpResponse, Http404,HttpResponseRedirect,HttpRequest
# Template
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from business_info.models import Stage,BusinessInfo
from core.models import Company
from django.http import HttpResponse
from django.views.generic import ListView
from django.utils import translation
from django.db.models import Q
from core.models import Locations
from publications.models import Publications
from sites.models import Sites
from library.models import Library
from django.contrib.contenttypes.models import ContentType
# Create your views here.


class StageView(ListView):
    model = Stage
    template_name = 'business_info/stages.html'
    def get_context_data(self, *args, **kwargs):
        context = super(StageView, self).get_context_data(**kwargs)
        context['slug'] = Stage.objects.all()
        category_obj = Stage.objects.get(slug=self.kwargs['slug'])
        objectss = BusinessInfo.objects.filter(stage = category_obj.id)
        ctype = ContentType.objects.get(model='publications')
        publications = BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = category_obj.id))[:4]
        ctype = ContentType.objects.get(model='company')        
        companies = BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = category_obj.id))[:4]
        ctype = ContentType.objects.get(model='sites')
        sites  = BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = category_obj.id))[:4]
        ctype = ContentType.objects.get(model='library')
        libraries  =BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = category_obj.id))[:4]
        sites_list = []
        comp_list = []
        pub_list =[]
        library_list =[]
        a = 1
        for y in publications:
            pub_list.append(y.object_id)
        for y in companies:
            comp_list.append(y.object_id)
        for y in sites:
            sites_list.append(y.object_id)
        for y in libraries:
                    library_list.append(y.object_id)

        publication = Publications.objects.filter(id__in= pub_list)
        company = Company.objects.filter(id__in= comp_list)
        site = Sites.objects.filter(id__in= sites_list)
        library = Library.objects.filter(id__in= library_list)



        # category_ob = Stage.objects.filter(parent_id=category_obj.id)

        cur_language = translation.get_language()
        context['cur_language'] = cur_language
        context['stages'] = category_obj
        context['publications'] = publication
        context['companies'] = company
        context['sites'] = site
        context['libraries'] = library
        context['objects'] = objectss

    # context['vacancy_list']=Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        return context

class CompanyView(ListView):
    model = Company
    template_name = 'catalog/last_category.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CompanyView, self).get_context_data(**kwargs)
        context['slug'] = Stage.objects.all()
        category_obj = Stage.objects.get(slug=self.kwargs['slug'])
#         objectss = BusinessInfo.objects.filter(stage = category_obj.id)
        ctype = ContentType.objects.get(model='company')
        companies =BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = category_obj.id))

        comp_list =[]

        for y in companies:
            comp_list.append(y.object_id)

        company = Company.objects.filter(id__in= comp_list)
        context['companies'] = company

        return context
