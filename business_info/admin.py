from django.contrib import admin
from business_info.models import BusinessInfo,Stage
from business_info.forms import BusinessInfoForm,StageForm


# Register your models here.


class BusinessInfoAdmin(admin.ModelAdmin):

    form = BusinessInfoForm
    list_display = ('stage','content_link',)

admin.site.register(BusinessInfo,BusinessInfoAdmin)

class StageAdmin(admin.ModelAdmin):

    form = StageForm
    prepopulated_fields = {'slug': ('name',),}
    list_display = ('name','slug','description')


admin.site.register(Stage,StageAdmin)