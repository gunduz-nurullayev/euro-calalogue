from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login, logout
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from django.forms import ModelForm
from business_info.models import BusinessInfo,Stage



class BusinessInfoForm(forms.ModelForm):
    # body = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = BusinessInfo
        fields = '__all__'


class StageForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Stage
        fields = '__all__'
