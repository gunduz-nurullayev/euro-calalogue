from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from core.models import Locations,Categories,Company
from publications.models import Publications
from sites.models import Sites

# from django.contrib.contenttypes.generic import GenericRelations

# Create your models here.
#
class Stage(models.Model):
    name = models.CharField(_('Name'),max_length = 255)
    slug = models.SlugField(_('Slug'))
    description = models.CharField(_('Description'),max_length = 255)


    class Meta:
        verbose_name = _('Stage')
        verbose_name_plural = _('Stages')

    def __unicode__(self):
        return self.name


class BusinessInfo(models.Model):

    location = models.ForeignKey(Locations)
    stage = models.ForeignKey(Stage)
    content_link = models.ForeignKey(ContentType,related_name = 'businessinfo_content')
    # content_link =  ContentType.objects.get_for_model(Company,Publications)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_link', 'object_id')

    class Meta:
        verbose_name = _('Business Info')
        verbose_name_plural = _('Business Info')


    # def __unicode__(self):              # __unicode__ on Python 2
    #     return self.stage

    # category = models.ForeignKey(Categories)
