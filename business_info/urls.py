from django.conf.urls import patterns, url
from business_info import views
from library.views import LibraryView
# from workspace import views as wp_views


urlpatterns = patterns('',

   # url(r'^/$',views.CompanyView.as_view(),  name='company_view'),
   url(r'^(?P<slug>.*?)/companies/$',views.CompanyView.as_view(),  name='stage_company_view'),
   url(r'^(?P<slug>.*?)/library/$',LibraryView.as_view(),  name='stage_library_view'),

   url(r'^(?P<slug>.*?)/$',views.StageView.as_view(),  name='stage_view'),


    #user actions

)