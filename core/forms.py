# from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from core.models import Company,CompanyCapacity,CompanyCertificates,CompanyClients,CompanyEmployees,CompanyPartners,Categories,\
    CompanyPartnership,CompanyProducts,CompanyProfile,CompanyProjects,CompanyTechnology,CompanyType,Locations,\
    User,CustomUserManager,UserPasswordRecovery,UserProfile
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from django.contrib.auth import authenticate, login, logout
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.forms import ModelForm
from django.contrib.admin import widgets

from django.forms.extras.widgets import SelectDateWidget
from django.contrib.admin.widgets import AdminDateWidget
from functools import partial
DateInput = partial(forms.DateInput,{'class': 'datepicker'})
#Locations
#
#
#
#                                   Company Object Don't Disturb
#
class CompanyTypeForm(forms.ModelForm):

    class Meta:
        model = CompanyType
        fields = '__all__'

class LocationsForm(forms.ModelForm):
    class Meta:
        model = Locations
        fields = '__all__'


from django.forms.models import inlineformset_factory
# CompanyForm
class CompanyForm(forms.ModelForm):

    description = forms.CharField(widget=CKEditorWidget())
    date = forms.DateField(required=False)
    last_check_date = forms.DateField(required=False)
    class Meta:
        model = Company
        fields = '__all__'
        # fields = ['name','slug','url','description','city','location','address','phone','fax','email','logo','spoken_languages',\
        #           'workingcountries','status','clicks','last_check_date','file_name','comp_type','date','category']
    def __init__(self, *args, **kwargs):
        super(CompanyForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})
        # self.fields['date'] = forms.DateTimeField(required=False)
        self.fields['last_check_date']= forms.DateTimeField(required=False)
        self.fields['category'].queryset = Categories.objects.filter(level=3)
        self.fields['category'].helptext = "Use 'Ctrl' for select many categories"
        # self.fields['last_check_date'] = forms.DateField(auto_now_add=True)
        # self.add_fields()
        # self.fields['comp_type'].widget = forms.CheckboxSelectMultiple()
        # comp_type = CompanyType.objects.filter()
        # self.fields['comp_type'].queryset = comp_type
        self.fields["comp_type"].queryset = CompanyType.objects.all()
        # self.fields["comp_type"] = forms.ModelMultipleChoiceField(
           # widget=forms.CheckboxSelectMultiple(),
        # required=False)
        # self.fields["comp_type"].widget = forms.CheckboxSelectMultiple()
        # self.fields["comp_type"].queryset = CompanyType.objects.all()

    #
    # def add_fields(self):
    #     # generate a queryset of the activities to show
    #     # could be sorted if a special order is needed
    #     comptype = CompanyType.objects.all()
    #     if not comptype:
    #         return

        # for a in comptype:
        #     # required false is important to allow the field NOT to be checked
        #     self.fields[a.id] = forms.BooleanField(label=a.name, required=False)

    # CompanyForm = inlineformset_factory(CompanyType, Company)
# CompanyTechnologyForm
class CompanyTechnologyForm(forms.ModelForm):
    equipment = forms.CharField(widget=CKEditorWidget())
    it = forms.CharField(widget=CKEditorWidget())
    resanddev = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyTechnology
        fields = '__all__'

# CompanyProjectsForm
class CompanyProjectsForm(forms.ModelForm):
    begin_date = forms.DateField(widget=DateInput())
    end_date = forms.DateField(widget=DateInput())

    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyProjects
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CompanyProjectsForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})

# CompanyProfileForm
class CompanyProfileForm(forms.ModelForm):

    mission = forms.CharField(widget=CKEditorWidget())
    history = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyProfile
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CompanyProfileForm, self).__init__(*args, **kwargs)
        self.fields['mission'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})
        self.fields['history'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})

# CompanyProductsForm
class CompanyProductsForm(forms.ModelForm):
    date = forms.DateField(widget=DateInput())

    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyProducts
        fields =['name','description','photo','link','date']
    def __init__(self, *args, **kwargs):
        super(CompanyProductsForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})

# CompanyPartnership
class CompanyPartnershipForm(forms.ModelForm):

    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyPartnership
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CompanyPartnershipForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})



# CompanyPartners
class CompanyPartnersForm(forms.ModelForm):
    since = forms.DateField(widget=DateInput())

    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyPartners
        fields =['partner','partnertype','description','since','photo','link']

    def __init__(self, *args, **kwargs):
        super(CompanyPartnersForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})

        # since = forms.DateField(widget=AdminDateWidget())



# CompanyEmployees
class CompanyEmployeesForm(forms.ModelForm):

    about = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyEmployees
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CompanyEmployeesForm, self).__init__(*args, **kwargs)
        self.fields['about'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})

# CompanyClients
class CompanyClientsForm(forms.ModelForm):
    since = forms.DateField(widget=DateInput())

    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyClients
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CompanyClientsForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})

# CompanyCertificates
class CompanyCertificatesForm(forms.ModelForm):
    receive_year = forms.DateField(widget=DateInput())

    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyCertificates
        fields =['name','certification_company','description','receive_year','photo','filename']

    def __init__(self, *args, **kwargs):
        super(CompanyCertificatesForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget = CKEditorWidget(attrs={'class':'ckeditor','cols': 50, 'rows': 15, 'style': 'resize:none;'})

class CompanyCapacityForm(forms.ModelForm):
    # receive_year = forms.DateField(widget=DateInput())
    #
    # description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = CompanyCapacity
        fields =['manufempl','manageempl','volume','production_space']

