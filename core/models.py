from django.db import models
from django.utils.translation import ugettext as _
import os
from catalog.models import Categories
from easy_thumbnails.fields import ThumbnailerImageField
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.contrib.auth.hashers import (
    check_password, make_password, is_password_usable)

import datetime
from django.dispatch import receiver
from django.template.defaultfilters import slugify
# Create your models here.

class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError(_('The given email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser,
                          last_login = now,
                          date_joined=now, **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)

class CompanyType(models.Model):
    name = models.CharField(_('Name'),max_length=100)

    def __unicode__(self):
        return self.name

class Locations(models.Model):
    parent = models.CharField(_('Parent ID'),max_length = 50)

    title = models.CharField(_('Title'),max_length = 100)
    code = models.CharField(_('Code'),max_length = 3)
    path = models.CharField(_('Path'),max_length = 50)
    status = models.CharField(_('Status'),max_length = 50)
    code2 = models.CharField(_('Code2'),max_length=2)

    class Meta:
        verbose_name = _('Location')
        verbose_name_plural = _('Locations')

    def __unicode__(self):
        return self.title

class Company(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.slug, "company", filename)

    name = models.CharField(_('Name'),max_length=255)
    slug = models.SlugField(_('Slug'),db_index=True,unique=True,max_length=255)
    url = models.URLField(_('Web Site'),null=True, blank=True)
    description = models.TextField(_('Description'),null=True, blank=True)
    city = models.CharField(_('City'),max_length=255,null=True, blank=True)
    address = models.CharField(_('Address'),max_length=255,null=True, blank=True)
    phone = models.CharField(_('Phone'),max_length=255,null=True, blank=True)
    fax = models.CharField(_('Fax'),max_length=255,null=True, blank=True)
    email = models.EmailField(_('Company Email'),max_length=255,null=True, blank=True)

    # logo = models.ImageField(_('Logo'),upload_to = 'core/company/imgs')
    logo = ThumbnailerImageField('Logo', upload_to=get_upload_path,max_length=255,null=True, blank=True)

    spoken_languages = models.CharField(_('Spoken Lanuguages'),max_length=255,null=True, blank=True)
    workingcountries = models.CharField(_('Working Countries'),max_length=255,null=True, blank=True)
    status = models.CharField(_('Status'),max_length = 50,null=True, blank=True)
    date = models.DateTimeField(_('Date'),null=True,blank=True,editable=False)
    clicks = models.PositiveIntegerField(_('Clicks'), default=0,blank=True, null=True)
    last_check_date = models.DateTimeField(_('Last Check Date'),null=True,blank=True)
    file_name = models.FileField(_('File'),upload_to = 'core/company/files',max_length=255,null=True, blank=True)

    # comp_type = models.ForeignKey(CompanyType,null=True, blank=True)
    comp_type = models.ManyToManyField(CompanyType)
    # account_id = models.ForeignKey()
    category = models.ManyToManyField(Categories,'Category')
    location = models.ForeignKey(Locations,null=True, blank=True)

    # objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []
    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        # if not self.id:
            # self.created = datetime.datetime.today()
        self.last_check_date = datetime.datetime.today()
        return super(Company, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Company, self).save(*args, **kwargs)


class User(AbstractBaseUser, PermissionsMixin):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.slug, "company", filename)

    company = models.ForeignKey(Company, related_name='users', blank=True, null=True)
    #User personal info
    email = models.EmailField(_('Email'), unique=True, db_index=True)

    # name = models.CharField(_('Company Name'),max_length=255)
    # slug = models.SlugField(_('Slug'))
    date_joined = models.DateTimeField(_('Date joined'), default=timezone.now)
    type = models.CharField(_('Type'),max_length=10, blank=True, null=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    # url = models.URLField(_('Web Site'))
    # description = models.TextField(_('Description'))
    # city = models.CharField(_('City'), max_length=255)
    # address = models.CharField(_('Address'),max_length=255)
    # phone = models.CharField(_('Phone'),max_length=100)
    # fax = models.CharField(_('Fax'),max_length=100)
    #
    # # logo = models.ImageField(_('Logo'),upload_to = 'core/company/imgs')
    # logo = ThumbnailerImageField('Logo', upload_to=get_upload_path,)
    #
    # spoken_languages = models.CharField(_('Spoken Lanuguages'),max_length=255)
    # workingcountires = models.CharField(_('Working Countries'),max_length=255)
    # status = models.CharField(_('Status'),max_length = 50)
    # date = models.DateTimeField(_('Date joined'),auto_now_add=True, blank=True)
    # clicks = models.PositiveIntegerField(_('Clicks'))
    # last_check_date = models.DateTimeField(_('Last Check Date'),auto_now_add=True, blank=True)
    # file_name = models.FileField(_('File'),upload_to = 'core/company/files')
    #
    # comp_type = models.ForeignKey(CompanyType)
    # # account_id = models.ForeignKey()
    # category = models.ManyToManyField(Categories,'Category')
    # location = models.ForeignKey(Locations)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    def __unicode__(self):
        return self.email

    # def get_absolute_url(self):
    #     return reverse('register:user_profile', kwargs={
    #                                 'id': self.id})


    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def get_full_name(self,company):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        # full_name = '%s %s' % (self.first_name, self.last_name)
        return self.company.name

    def get_short_name(self,company):
        "Returns the short name for the user."
        return self.company.name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

#This class has used to confirm registration
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=50)
    key_expires = models.DateTimeField()

#This class has used to set new password when user forget password
class UserPasswordRecovery(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=50)
    key_expires = models.DateTimeField()

class CompanyTechnology(models.Model):
    company = models.ForeignKey(Company)
    equipment = models.TextField(_('Equipment'),null =True,blank=True)
    it = models.TextField(_('IT'),null =True,blank=True)
    resanddev = models.TextField(_('Resanddev'),null =True,blank=True)

    class Meta:
        verbose_name = _('Company Technology')
        verbose_name_plural = _('Company Technologies')


class CompanyProjects(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.company.slug, "projects", filename)

    company = models.ForeignKey(Company)
    name = models.CharField(_('Name'),max_length = 255)
    description = models.TextField(_('Description'),max_length = 255,null =True)

    # photo= ThumbnailerImageField('Photo', upload_to=get_upload_path, blank=True, null=True)

    photo = models.ImageField(_('Photo'),upload_to = 'core/company_projects',max_length = 255,null =True)
    link = models.URLField(_('Link'),max_length = 255,null =True)
    project_type = models.CharField(_('Project Type'),max_length = 255,null =True)
    location = models.CharField(_('Location'),max_length = 255,null =True)
    client = models.CharField(_('Client'),max_length = 255,null =True)
    role = models.CharField(_('Role'),max_length = 255,null =True)
    begin_date = models.DateTimeField(_('Begin Date'),max_length = 255,null =True)
    end_date = models.DateTimeField(_('End Date'),max_length = 255,null =True)

    class Meta:
        verbose_name = _('Company Project')
        verbose_name_plural = _('Company Projects')



class CompanyProfile(models.Model):
    # def create_user_profile(sender, instance, **kwargs):
    #     profile = CompanyProfile(company = instance)
    #     profile.save()
    company = models.ForeignKey(Company)
    mission = models.TextField(_('Mission'),null=True,blank=True)
    history = models.TextField(_('History'  ),null =True,blank=True)

    class Meta:
        verbose_name = _('Company Profile')
        verbose_name_plural = _('Company Profiles')

    # def create_user_profile(sender, instance, **kwargs):
        # profile = UserProfile(user = instance, subdomain = instance.username, ...)
        # profile.save()


class CompanyProducts(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.company.slug, "products", filename)

    company = models.ForeignKey(Company,related_name='company_products')
    name = models.CharField(_('Name'),max_length = 255)
    description = models.TextField(_('Description'),null =True,blank=True)
    # photo = models.ImageField(_('Photo'),upload_to = 'core/company_products')
    photo= ThumbnailerImageField('Photo', upload_to=get_upload_path, blank=True, null=True)
    date = models.DateField(_('Since'),null =True,blank=True)

    link = models.URLField(_('Link'),max_length=255,null =True,blank=True)

    class Meta:
        verbose_name = _('Company Product')
        verbose_name_plural = _('Company Products')


class CompanyPartnership(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.company.slug, "partnership", filename)

    company = models.ForeignKey(Company)
    name = models.CharField(_('Name'),max_length = 255)
    description = models.TextField(_('Description'))
    # program  = models.CharField(_('Program'),max_length = 255,null =True)
    file_name = models.FileField(_('File'),upload_to = 'core/company_partnership',null =True)
    # requirement = models.CharField('Requirements',max_length =255,null =True)
    link = models.URLField(_('Link'))

    class Meta:
        verbose_name = _('Company Partnership')
        verbose_name_plural = _('Company Partnerships')

class CompanyPartners(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.company.slug, "partners", filename)

    company = models.ForeignKey(Company)
    partner = models.CharField(_('Partner'),max_length = 255)
    partnertype = models.CharField(_('Partner Type'),max_length = 255,null=True,blank=True)
    description = models.TextField(_('Description'),null=True,blank=True)
    since = models.DateField(_('Since'),null=True,blank=True)
    photo = models.ImageField(_('Photo'),upload_to = 'core/company_partners',null=True,blank=True)
    link = models.URLField(_('Link'),null=True,blank=True)

    class Meta:
        verbose_name = _('Company Partner')
        verbose_name_plural = _('Company Partners')


class CompanyEmployees(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.company.slug, "employees", filename)

    company = models.ForeignKey(Company)
    fullname = models.CharField(_('Full Name'),max_length=255)
    position = models.CharField(_('Position'),max_length=255,null =True,blank=True)
    photo = models.ImageField(_('Photo'),upload_to = 'core/company_employees',null =True,blank=True)
    about = models.TextField(_('About'),null =True,blank=True)

    class Meta:
        verbose_name = _('Company Employee')
        verbose_name_plural = _('Company Employees')


class CompanyClients(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.company.slug, "clients", filename)

    company = models.ForeignKey(Company)
    client = models.CharField(_('Client'),max_length = 255)
    product_service = models.CharField(_('Product Service'),max_length=255)
    description = models.TextField(_('Description'))
    since = models.DateField(_('Since'),null =True)
    photo = models.ImageField(_('Photo'),upload_to = 'core/company_clients',null =True)
    link = models.URLField(_('Link'))

    class Meta:
        verbose_name = _('Company Client')
        verbose_name_plural = _('Company Clients')


class CompanyCertificates(models.Model):
    def get_upload_path(instance, filename):
        return os.path.join("companies", instance.company.slug, "certificates", filename)

    company = models.ForeignKey(Company)
    name = models.CharField(_('Name'),max_length = 255)
    certification_company = models.CharField(_('Certification Company'),max_length = 255)
    description = models.TextField(_('Description'))
    receive_year = models.DateField(_('Receive Year'),null =True)
    photo = models.ImageField(_('Photo'),upload_to = 'core/company_certificates/imgs',null =True)
    filename = models.FileField(_('File'),upload_to = 'core/company_certificates/files',null =True)

    class Meta:
        verbose_name = _('Company Certificate')
        verbose_name_plural = _('Company Certificates')

class CompanyCapacity(models.Model):
    company = models.ForeignKey(Company)
    manufempl = models.CharField(_('Manufacturing Employees'),max_length=255,null =True,blank=True)
    manageempl = models.CharField(_('Manager Employees'),max_length=255,null =True,blank=True)
    volume = models.CharField(_('Volume'),max_length=255,null =True,blank=True)
    production_space = models.CharField(_('Production Space'),max_length=255,null =True,blank=True)

    class Meta:
        verbose_name = _('Company Capacity')
        verbose_name_plural = _('Company Capacities')

