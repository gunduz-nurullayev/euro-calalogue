# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CompanyType'
        db.create_table(u'core_companytype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['CompanyType'])

        # Adding model 'Locations'
        db.create_table(u'core_locations', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'core', ['Locations'])

        # Adding model 'Company'
        db.create_table(u'core_company', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('spoken_languages', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('workingcountries', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('clicks', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('last_check_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('file_name', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('comp_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.CompanyType'])),
            ('location_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Locations'])),
        ))
        db.send_create_signal(u'core', ['Company'])

        # Adding M2M table for field category_id on 'Company'
        m2m_table_name = db.shorten_name(u'core_company_category_id')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('company', models.ForeignKey(orm[u'core.company'], null=False)),
            ('categories', models.ForeignKey(orm[u'catalog.categories'], null=False))
        ))
        db.create_unique(m2m_table_name, ['company_id', 'categories_id'])

        # Adding model 'CompanyTechnology'
        db.create_table(u'core_companytechnology', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('equipment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('resanddev', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'core', ['CompanyTechnology'])

        # Adding model 'CompanyProjects'
        db.create_table(u'core_companyprojects', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('project_type', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('client', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('begin_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'core', ['CompanyProjects'])

        # Adding model 'CompanyProfile'
        db.create_table(u'core_companyprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('mission', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('history', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'core', ['CompanyProfile'])

        # Adding model 'CompanyProducts'
        db.create_table(u'core_companyproducts', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'core', ['CompanyProducts'])

        # Adding model 'CompanyPartnership'
        db.create_table(u'core_companypartnership', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('program', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('file_name', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True)),
            ('requirement', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'core', ['CompanyPartnership'])

        # Adding model 'CompanyPartners'
        db.create_table(u'core_companypartners', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('partner', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('partnertype', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('since', self.gf('django.db.models.fields.DateField')(null=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['CompanyPartners'])

        # Adding model 'CompanyEmployees'
        db.create_table(u'core_companyemployees', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('fullname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('about', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'core', ['CompanyEmployees'])

        # Adding model 'CompanyClients'
        db.create_table(u'core_companyclients', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('client', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('product_service', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('since', self.gf('django.db.models.fields.DateField')(null=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'core', ['CompanyClients'])

        # Adding model 'CompanyCertificates'
        db.create_table(u'core_companycertificates', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('certification_company', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('receive_year', self.gf('django.db.models.fields.DateField')(null=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('filename', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'core', ['CompanyCertificates'])

        # Adding model 'CompanyCapacity'
        db.create_table(u'core_companycapacity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('manufempl', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('manageempl', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('volume', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('production_space', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'core', ['CompanyCapacity'])


    def backwards(self, orm):
        # Deleting model 'CompanyType'
        db.delete_table(u'core_companytype')

        # Deleting model 'Locations'
        db.delete_table(u'core_locations')

        # Deleting model 'Company'
        db.delete_table(u'core_company')

        # Removing M2M table for field category_id on 'Company'
        db.delete_table(db.shorten_name(u'core_company_category_id'))

        # Deleting model 'CompanyTechnology'
        db.delete_table(u'core_companytechnology')

        # Deleting model 'CompanyProjects'
        db.delete_table(u'core_companyprojects')

        # Deleting model 'CompanyProfile'
        db.delete_table(u'core_companyprofile')

        # Deleting model 'CompanyProducts'
        db.delete_table(u'core_companyproducts')

        # Deleting model 'CompanyPartnership'
        db.delete_table(u'core_companypartnership')

        # Deleting model 'CompanyPartners'
        db.delete_table(u'core_companypartners')

        # Deleting model 'CompanyEmployees'
        db.delete_table(u'core_companyemployees')

        # Deleting model 'CompanyClients'
        db.delete_table(u'core_companyclients')

        # Deleting model 'CompanyCertificates'
        db.delete_table(u'core_companycertificates')

        # Deleting model 'CompanyCapacity'
        db.delete_table(u'core_companycapacity')


    models = {
        u'catalog.categories': {
            'Meta': {'object_name': 'Categories'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'meta_keyword': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'page_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Categories']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'category_id': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['catalog.Categories']", 'symmetrical': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'clicks': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comp_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.CompanyType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'file_name': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_check_date': ('django.db.models.fields.DateTimeField', [], {}),
            'location_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Locations']"}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'spoken_languages': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'workingcountries': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.companycapacity': {
            'Meta': {'object_name': 'CompanyCapacity'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manageempl': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'manufempl': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'production_space': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'volume': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.companycertificates': {
            'Meta': {'object_name': 'CompanyCertificates'},
            'certification_company': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'filename': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'receive_year': ('django.db.models.fields.DateField', [], {'null': 'True'})
        },
        u'core.companyclients': {
            'Meta': {'object_name': 'CompanyClients'},
            'client': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'product_service': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'since': ('django.db.models.fields.DateField', [], {'null': 'True'})
        },
        u'core.companyemployees': {
            'Meta': {'object_name': 'CompanyEmployees'},
            'about': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'fullname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.companypartners': {
            'Meta': {'object_name': 'CompanyPartners'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'partner': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'partnertype': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'since': ('django.db.models.fields.DateField', [], {'null': 'True'})
        },
        u'core.companypartnership': {
            'Meta': {'object_name': 'CompanyPartnership'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'file_name': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'program': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'requirement': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'core.companyproducts': {
            'Meta': {'object_name': 'CompanyProducts'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'core.companyprofile': {
            'Meta': {'object_name': 'CompanyProfile'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'history': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mission': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.companyprojects': {
            'Meta': {'object_name': 'CompanyProjects'},
            'begin_date': ('django.db.models.fields.DateTimeField', [], {}),
            'client': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'project_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'core.companytechnology': {
            'Meta': {'object_name': 'CompanyTechnology'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'equipment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'resanddev': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'core.companytype': {
            'Meta': {'object_name': 'CompanyType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.locations': {
            'Meta': {'object_name': 'Locations'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['core']