# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table(u'core_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['core.User'], unique=True)),
            ('activation_key', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('key_expires', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'core', ['UserProfile'])

        # Adding model 'User'
        db.create_table(u'core_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='users', null=True, to=orm['core.Company'])),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'core', ['User'])

        # Adding M2M table for field groups on 'User'
        m2m_table_name = db.shorten_name(u'core_user_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'core.user'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'User'
        m2m_table_name = db.shorten_name(u'core_user_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'core.user'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'permission_id'])

        # Adding model 'UserPasswordRecovery'
        db.create_table(u'core_userpasswordrecovery', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['core.User'], unique=True)),
            ('activation_key', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('key_expires', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'core', ['UserPasswordRecovery'])

        # Deleting field 'Company.location_id'
        db.delete_column(u'core_company', 'location_id')

        # Adding field 'Company.location'
        db.add_column(u'core_company', 'location',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Locations'], null=True, blank=True),
                      keep_default=False)

        # Removing M2M table for field category_id on 'Company'
        db.delete_table(db.shorten_name(u'core_company_category'))

        # Adding M2M table for field category on 'Company'
        m2m_table_name = db.shorten_name(u'core_company_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('company', models.ForeignKey(orm[u'core.company'], null=False)),
            ('categories', models.ForeignKey(orm[u'catalog.categories'], null=False))
        ))
        db.create_unique(m2m_table_name, ['company_id', 'categories_id'])


        # Changing field 'Company.status'
        db.alter_column(u'core_company', 'status', self.gf('django.db.models.fields.CharField')(max_length=50, null=True))

        # Changing field 'Company.city'
        db.alter_column(u'core_company', 'city', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Company.fax'
        db.alter_column(u'core_company', 'fax', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Company.last_check_date'
        db.alter_column(u'core_company', 'last_check_date', self.gf('django.db.models.fields.DateTimeField')(null=True))

        # Changing field 'Company.description'
        db.alter_column(u'core_company', 'description', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Company.workingcountries'
        db.alter_column(u'core_company', 'workingcountries', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Company.url'
        db.alter_column(u'core_company', 'url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True))

        # Changing field 'Company.address'
        db.alter_column(u'core_company', 'address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Company.spoken_languages'
        db.alter_column(u'core_company', 'spoken_languages', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Company.phone'
        db.alter_column(u'core_company', 'phone', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Company.comp_type'
        db.alter_column(u'core_company', 'comp_type_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.CompanyType'], null=True))

        # Changing field 'Company.date'
        db.alter_column(u'core_company', 'date', self.gf('django.db.models.fields.DateTimeField')(null=True))

        # Changing field 'Company.clicks'
        db.alter_column(u'core_company', 'clicks', self.gf('django.db.models.fields.PositiveIntegerField')(null=True))

    def backwards(self, orm):
        # Deleting model 'UserProfile'
        db.delete_table(u'core_userprofile')

        # Deleting model 'User'
        db.delete_table(u'core_user')

        # Removing M2M table for field groups on 'User'
        db.delete_table(db.shorten_name(u'core_user_groups'))

        # Removing M2M table for field user_permissions on 'User'
        db.delete_table(db.shorten_name(u'core_user_user_permissions'))

        # Deleting model 'UserPasswordRecovery'
        db.delete_table(u'core_userpasswordrecovery')

        # Adding field 'Company.location_id'
        db.add_column(u'core_company', 'location_id',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=2, to=orm['core.Locations']),
                      keep_default=False)

        # Deleting field 'Company.location'
        db.delete_column(u'core_company', 'location_id')

        # Adding M2M table for field category_id on 'Company'
        m2m_table_name = db.shorten_name(u'core_company_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('company', models.ForeignKey(orm[u'core.company'], null=False)),
            ('categories', models.ForeignKey(orm[u'catalog.categories'], null=False))
        ))
        db.create_unique(m2m_table_name, ['company_id', 'categories_id'])

        # Removing M2M table for field category on 'Company'
        db.delete_table(db.shorten_name(u'core_company_category'))


        # Changing field 'Company.status'
        db.alter_column(u'core_company', 'status', self.gf('django.db.models.fields.CharField')(default=2, max_length=50))

        # Changing field 'Company.city'
        db.alter_column(u'core_company', 'city', self.gf('django.db.models.fields.CharField')(default=2, max_length=255))

        # Changing field 'Company.fax'
        db.alter_column(u'core_company', 'fax', self.gf('django.db.models.fields.CharField')(default=2, max_length=100))

        # Changing field 'Company.last_check_date'
        db.alter_column(u'core_company', 'last_check_date', self.gf('django.db.models.fields.DateTimeField')(default=2))

        # Changing field 'Company.description'
        db.alter_column(u'core_company', 'description', self.gf('django.db.models.fields.TextField')(default=2))

        # Changing field 'Company.workingcountries'
        db.alter_column(u'core_company', 'workingcountries', self.gf('django.db.models.fields.CharField')(default=2, max_length=255))

        # Changing field 'Company.url'
        db.alter_column(u'core_company', 'url', self.gf('django.db.models.fields.URLField')(default=2, max_length=200))

        # Changing field 'Company.address'
        db.alter_column(u'core_company', 'address', self.gf('django.db.models.fields.CharField')(default=2, max_length=255))

        # Changing field 'Company.spoken_languages'
        db.alter_column(u'core_company', 'spoken_languages', self.gf('django.db.models.fields.CharField')(default=2, max_length=255))

        # Changing field 'Company.phone'
        db.alter_column(u'core_company', 'phone', self.gf('django.db.models.fields.CharField')(default=2, max_length=100))

        # Changing field 'Company.comp_type'
        db.alter_column(u'core_company', 'comp_type_id', self.gf('django.db.models.fields.related.ForeignKey')(default=2, to=orm['core.CompanyType']))

        # Changing field 'Company.date'
        db.alter_column(u'core_company', 'date', self.gf('django.db.models.fields.DateTimeField')(default=2))

        # Changing field 'Company.clicks'
        db.alter_column(u'core_company', 'clicks', self.gf('django.db.models.fields.PositiveIntegerField')(default=2))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'catalog.categories': {
            'Meta': {'object_name': 'Categories'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'meta_keyword': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'page_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Categories']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['catalog.Categories']", 'symmetrical': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'clicks': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'comp_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.CompanyType']", 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'file_name': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_check_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Locations']", 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'spoken_languages': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'workingcountries': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'core.companycapacity': {
            'Meta': {'object_name': 'CompanyCapacity'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manageempl': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'manufempl': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'production_space': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'volume': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.companycertificates': {
            'Meta': {'object_name': 'CompanyCertificates'},
            'certification_company': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'filename': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'receive_year': ('django.db.models.fields.DateField', [], {'null': 'True'})
        },
        u'core.companyclients': {
            'Meta': {'object_name': 'CompanyClients'},
            'client': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'product_service': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'since': ('django.db.models.fields.DateField', [], {'null': 'True'})
        },
        u'core.companyemployees': {
            'Meta': {'object_name': 'CompanyEmployees'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'fullname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.companypartners': {
            'Meta': {'object_name': 'CompanyPartners'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'partner': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'partnertype': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'since': ('django.db.models.fields.DateField', [], {'null': 'True'})
        },
        u'core.companypartnership': {
            'Meta': {'object_name': 'CompanyPartnership'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'file_name': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.companyproducts': {
            'Meta': {'object_name': 'CompanyProducts'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'company_products'", 'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'core.companyprofile': {
            'Meta': {'object_name': 'CompanyProfile'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'history': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mission': ('django.db.models.fields.TextField', [], {})
        },
        u'core.companyprojects': {
            'Meta': {'object_name': 'CompanyProjects'},
            'begin_date': ('django.db.models.fields.DateTimeField', [], {}),
            'client': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'project_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'core.companytechnology': {
            'Meta': {'object_name': 'CompanyTechnology'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'equipment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'resanddev': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'core.companytype': {
            'Meta': {'object_name': 'CompanyType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.locations': {
            'Meta': {'object_name': 'Locations'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.user': {
            'Meta': {'object_name': 'User'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'users'", 'null': 'True', 'to': u"orm['core.Company']"}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"})
        },
        u'core.userpasswordrecovery': {
            'Meta': {'object_name': 'UserPasswordRecovery'},
            'activation_key': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key_expires': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['core.User']", 'unique': 'True'})
        },
        u'core.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'activation_key': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key_expires': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['core.User']", 'unique': 'True'})
        }
    }

    complete_apps = ['core']