from django.views import generic as generic_views
# from django.views import FormMixin, ListView
# from . import models,settings
# from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext
# Http
from django.http import HttpResponse, Http404,HttpResponseRedirect
#json
from django.core.serializers.json import DjangoJSONEncoder
import json
#mail
from django.core.mail import send_mail
#settings
from eurocatalog.settings import EMAIL_HOST_USER, PORTAL_URL
# models
#form
from company_account.forms import RegisterForm
from django.shortcuts import render,render_to_response
from django.http import HttpResponse
from django.shortcuts import render,redirect,get_object_or_404
# Create your views here.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BOOK CATEGORY LIST~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from django.http import HttpResponseRedirect,Http404
# from .forms import UploadFileForm
from django.utils import translation
from django.utils import timezone
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core.context_processors import csrf
from django.core.mail import send_mail, BadHeaderError
# from universus_public.settings import EMAIL_HOST_USER
from django.db.models import Q
# Create your views here.
# from core.models import User
from django.views.generic.edit import FormMixin
from django.views.generic.list import ListView

from news.models import News
from publications.models import Publications
from sites.models import Sites
from exhibitions.models import Exhibitions
from core.models import Locations,Company,CompanyProfile,CompanyProducts,CompanyProjects,CompanyPartnership,\
    CompanyPartners,CompanyClients,CompanyEmployees,UserProfile,UserPasswordRecovery,CustomUserManager,User
from business_info.models import Stage,BusinessInfo
#python
import datetime, random, sha
# Authentication
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from catalog.models import Categories
from django.contrib.contenttypes.models import ContentType

# Create your views here.


#COmpany View
class CompanyView(ListView):
    model = Company
    template_name = 'company/company_detail.html'
    def get_context_data(self, *args, **kwargs):
        context = super(CompanyView, self).get_context_data(**kwargs)

        context['slug'] = Company.objects.all()
        company = Company.objects.get(slug=self.kwargs['slug'])
        about = CompanyProfile.objects.get(company=company.id)
        products = CompanyProducts.objects.filter(company=company.id)
        projects = CompanyProjects.objects.filter(company=company.id)
        partnerships = CompanyPartnership.objects.filter(company=company.id)
        partners = CompanyPartners.objects.filter(company=company.id)
        clients = CompanyClients.objects.filter(company=company.id)
        employees = CompanyEmployees.objects.filter(company=company.id)
        # company = company.url.replace(self,company.url.,1)
        # if not self.request.META['HTTP_REFERER']:
        #     url = ''
        #
        # else:
        # url = self.request.META['HTTP_REFERER']
        # context['default_url'] =
        # context['default_url'] = url
        context['company'] = company
        context['about'] = about
        context['products'] = products
        context['projects'] = projects
        context['partnerships'] = partnerships
        context['partners'] = partners
        context['clients'] = clients
        context['employees'] = employees

        cur_language = translation.get_language()
        context['cur_language'] = cur_language

        return context
from company_account.forms import LoginForm
# home page

def use_session(request):
    if request.GET.get('location_id'):
            request.session['location'] = request.GET['location_id']
            location = request.GET['location_id']
    if not 'location' in request.session:
            request.session['location'] = ''
    return  redirect('/')

class HomePageView(FormMixin, ListView):
    model = Company
    template_name = 'base.html'
    form_class =LoginForm
    # form_class = VacancyApplicationForm
    # success_url = '/thanks/'

    def get(self, request, *args, **kwargs):
        # context = super(HomePageView, self).get_context_data(**kwargs)

# From ProcessFormMixin
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)

        if self.request.method == 'POST':
            # return HttpResponse(self.form.errors)
            self.form_class(request.POST)
            if form_class.is_valid():
                next = self.request.POST.get('next',False)
                email = form_class.cleaned_data["email"]
                password = form_class.cleaned_data["password"]
                user = authenticate(username=email, password=password)
                login(request, user)
            # Redirect to a success page.
                if next:
                    return redirect(next)

                return redirect('index')
            # if self.form.is_valid():
            #     obj = self.form.save(commit=False)
                # obj.save()
#             return HttpResponseRedirect('/thanks/')
#         else:
#             self.form_class(self.request.POST,request.FILES)

            # return super(ProductListView, self).form_valid(form)

        # From BaseListView
        self.object_list = self.get_queryset()
        context = self.get_context_data(object_list=self.object_list, form=self.form)
        if not 'location' in request.session:
            request.session['location'] = 0
        # y = ''
        # if request.GET.get('location_id'):
        #     request.session['location'] = request.GET['location_id']
        #     y = request.GET['location_id']
        # if not 'location' in request.session:
        #     request.session['location'] = ''

        location = request.session['location']
        # context['x'] =  y
        context['x'] =  int(location)
        sites_list = []
        comp_list = []
        pub_list =[]
        library_list =[]
        a = 1
        # Glossary_Stage =[]
        stages_number = [1,2,3,4,5,6,7,8]
        stage_dic = {1:['regionchoose','REGION CHOOSE'],2:['formation','BUSINESS FORMATION'],3:['investments','BUSINESS INVESTMENTS'],\
                     4:['market','MARKET RESEARCH'],5:['regional','REGIONAL PROMOTION'],6:['partners','PARTNERS SEARCH'],\
                     7:['representative','REPRESENTATIVE OFFICE'],8:['ready','READY BUSINESS']}
        stages ={}
        for object in stages_number:
            publications = ""
            companies = ""
            sites = ""
            libraries = ""
            stage_list = []
            if request.session['location']:
                ctype = ContentType.objects.get(model='publications')
                publications = BusinessInfo.objects.filter(Q(location_id=location) & Q(content_link = ctype) & Q(stage = object))[:1]
                ctype = ContentType.objects.get(model='company')
                companies = BusinessInfo.objects.filter(Q(location_id=location) & Q(content_link = ctype) & Q(stage = object))[:1]
                ctype = ContentType.objects.get(model='sites')
                sites  = BusinessInfo.objects.filter(Q(location_id=location) & Q(content_link = ctype) & Q(stage = object))[:1]
                ctype = ContentType.objects.get(model='library')
                libraries  = BusinessInfo.objects.filter(Q(location_id=location) & Q(content_link = ctype) & Q(stage = object))[:1]
            else:
                ctype = ContentType.objects.get(model='publications')
                publications = BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = object))[:1]
                ctype = ContentType.objects.get(model='company')
                companies = BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = object))[:1]
                ctype = ContentType.objects.get(model='sites')
                sites  = BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = object))[:1]
                ctype = ContentType.objects.get(model='library')
                libraries  = BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = object))[:1]

            links_dic = {}
            if publications:
                publication = Publications.objects.get(id=publications[0].object_id)
                if publication.location_id == location:
                    links_dic.update({'type':'publications'})
                    links_dic.update({'title':publication.title})
                    links_dic.update({'link':publication.slug})
                    stage_list.append(links_dic)

            links_dic = {}
            if companies:
                company = Company.objects.get(id=companies[0].object_id)
                links_dic.update({'type':'company'})
                links_dic.update({'title':company.name})
                links_dic.update({'link':company.slug})
                stage_list.append(links_dic)

            links_dic = {}
            if sites:
                site = Sites.objects.get(id=sites[0].object_id)
                links_dic.update({'type':'sites'})
                links_dic.update({'title':site.title})
                links_dic.update({'link':site.slug})
                stage_list.append(links_dic)

            links_dic = {}
            if libraries:
                library = Library.objects.get(id=libraries[0].object_id)
                links_dic.update({'type':'library'})
                links_dic.update({'title':library.title})
                links_dic.update({'link':library.slug})
                stage_list.append(links_dic)
            stages.update({object:stage_list})


        if request.session['location']:
            topcompanies = Company.objects.filter(location_id=location).order_by('-clicks')[:7]
            newcompanies = Company.objects.filter(location_id=location).order_by('date')[:7]
            news = News.objects.filter(location_id=location).order_by('-pub_date')[:4]
            last_publications = Publications.objects.filter(location_id=location).order_by('-senddate')[:3]
            upcoming_exhibitions = Exhibitions.objects.filter(location_id=location)[:3]

        else:
            topcompanies = Company.objects.all().order_by('-clicks')[:7]
            newcompanies = Company.objects.all().order_by('date')[:7]
            news = News.objects.all().order_by('-pub_date')[:4]
            last_publications = Publications.objects.all().order_by('-senddate')[:3]
            upcoming_exhibitions  = Exhibitions.objects.all()[:3]

        locations = Locations.objects.filter(~Q(parent = -1)).order_by('code')
        categories_count =Categories.objects.filter(level=3).count()
        company_count = Company.objects.all().count()
        publications_count =Publications.objects.all().count()

        # HTTPConnection.request(method, url[, body[, headers]])
        # selector = '{}?{}'.format(connections_strings.auth, params)
        context['stages_number'] =stages_number
        context['stages'] = stages
        context['stage_dic'] = stage_dic

        context['news'] = news
        context['publications'] = last_publications
        context['exhibitions'] = upcoming_exhibitions
        context['enterpreneurship'] = Stage.objects.all()
        context['topcompanies'] = topcompanies
        context['newcompanies'] = newcompanies
        context['locations'] = locations

        context['company_count'] = company_count
        context['categories_count'] = categories_count
        context['publications_count'] = publications_count
        del request.session['location']

        return self.render_to_response(context)

def index(request):
    # part_program = PartnershipProgram.objects.order_by("-update_date")[:4]
    news = News.objects.all().order_by('-pub_date')[:4]
    publications = Publications.objects.all().order_by('-senddate')[:3]
    exhibitions = Exhibitions.objects.all()[:3]
    cur_language = translation.get_language()

        # context['sites'] = Sites.objects.all()
    return render_to_response('base.html', {'news':news,'publications':publications,'exhibitions':exhibitions,'cur_language':cur_language},
                              context_instance=RequestContext(request))

from library.models import Library

def search(request):
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append('Enter a search term.')
        elif len(q) > 20:
            errors.append('Please enter at most 20 characters.')
        else:
            books = Library.objects.filter(title__icontains=q)
            return render(request, 'search/search_results.html',
                {'books': books, 'query': q})
    return render(request, 'search/search_form.html',
        {'errors': errors})