from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from core.models import Company,CompanyCapacity,CompanyCertificates,CompanyClients,CompanyEmployees,CompanyPartners,\
    CompanyPartnership,CompanyProducts,CompanyProfile,CompanyProjects,CompanyTechnology,CompanyType,Locations
from core.forms import CompanyForm,CompanyClientsForm,CompanyEmployeesForm,CompanyPartnersForm,CompanyPartnershipForm,CompanyTechnologyForm,CompanyTypeForm,\
    CompanyPartnersForm,CompanyPartnershipForm,CompanyProductsForm,CompanyProjectsForm,CompanyCertificatesForm,CompanyProfileForm,LocationsForm,User
from django.contrib.auth.admin import UserAdmin
from company_account.forms import CustomUserChangeForm, CustomUserCreationForm

class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        # (_('Personal info'), {'fields': ('first_name','last_name', 'country')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ('email','company', 'is_staff')
    search_fields = ('email', 'company')
    ordering = ('email',)

admin.site.register(User, CustomUserAdmin)


class CompanyTypeAdmin(admin.ModelAdmin):
    form = CompanyTypeForm
admin.site.register(CompanyType,CompanyTypeAdmin)
class LocationsAdmin(admin.ModelAdmin):
    form = LocationsForm

admin.site.register(Locations,LocationsAdmin)

class CompanyAdmin(admin.ModelAdmin):
    form = CompanyForm
    list_display = ('name','location', 'url','phone','clicks')

    # prepopulated_fields = {'slug': ('name',)}

admin.site.register(Company,CompanyAdmin)


class CompanyTechnologyAdmin(admin.ModelAdmin):
    list_display = ('company','equipment','it')

    form = CompanyTechnologyForm

admin.site.register(CompanyTechnology,CompanyTechnologyAdmin)


class CompanyProjectsAdmin(admin.ModelAdmin):
    list_display = ('company','name','client','role')

    form = CompanyProjectsForm

admin.site.register(CompanyProjects,CompanyProjectsAdmin)

class CompanyProfileAdmin(admin.ModelAdmin):
    list_display = ('company','mission')

    form = CompanyProfileForm

admin.site.register(CompanyProfile,CompanyProfileAdmin)

class CompanyProductsAdmin(admin.ModelAdmin):
    list_display = ('company','name','link')

    form = CompanyProductsForm

admin.site.register(CompanyProducts,CompanyProductsAdmin)

class CompanyPartnershipAdmin(admin.ModelAdmin):
    list_display = ('company','name','link')

    form = CompanyPartnershipForm

admin.site.register(CompanyPartnership,CompanyPartnershipAdmin)

class CompanyPartnersAdmin(admin.ModelAdmin):
    list_display = ('company','partner','partnertype','since')

    form = CompanyPartnersForm

admin.site.register(CompanyPartners,CompanyPartnersAdmin)


class CompanyEmployeesAdmin(admin.ModelAdmin):
    list_display = ('company','fullname','position')

    form = CompanyEmployeesForm

admin.site.register(CompanyEmployees,CompanyEmployeesAdmin)


class CompanyClientsAdmin(admin.ModelAdmin):
    list_display = ('company','client','product_service')

    form = CompanyClientsForm

admin.site.register(CompanyClients,CompanyClientsAdmin)


class CompanyCertificatesAdmin(admin.ModelAdmin):
    list_display = ('company','name','certification_company','receive_year')

    form = CompanyCertificatesForm

admin.site.register(CompanyCertificates,CompanyCertificatesAdmin)


