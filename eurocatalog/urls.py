from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings
from core.views import index,search,HomePageView,use_session
from django.contrib.staticfiles.urls import staticfiles_urlpatterns



admin.autodiscover() 
urlpatterns = i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),

    # url(r'^', include('core.urls', namespace='core')),
    url(r'^$',HomePageView.as_view(),  name='index'),
   url(r'^session/$',use_session,  name='use_session'),



    url(r'^search/$', search,  name='search'),
    url(r'^categories/', include('catalog.urls', namespace='catalog')),
    url(r'^company/', include('core.urls', namespace='company')),
    url(r'^news/', include('news.urls', namespace='news')),
    url(r'^publications/', include('publications.urls', namespace='publications')),
    url(r'^enterpreneurship/', include('business_info.urls', namespace='enterpreneurship')),
    url(r'^sites/', include('sites.urls', namespace='sites')),
    url(r'^exhibitions/', include('exhibitions.urls', namespace='exhibitions')),
    url(r'^library/',include('library.urls',namespace='library')),
    url(r'^account/',include('company_account.urls',namespace='company_account')),
)

# ... the rest of your URLconf goes here ...

urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns