from django.contrib import admin
from exhibitions.models import Exhibitions
from exhibitions.forms import ExhibitionsForm
# Register your models here.


class ExhibitionsAdmin(admin.ModelAdmin):
    form = ExhibitionsForm

    #     # date_hierarchy = 'pub_date'
    list_display = ('name', 'organizer','location','startdate','enddate')
#     #list_editable = ('title', 'is_published')
#     # list_filter = ('is_published', )
#     # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('name',)}
#     # form = LibraryForm

admin.site.register(Exhibitions,ExhibitionsAdmin)
