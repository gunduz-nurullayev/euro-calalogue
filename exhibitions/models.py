from django.db import models
from django.utils.translation import ugettext as _
from core.models import Locations
# Create your models here.


class Exhibitions(models.Model):
    name = models.CharField(_('Exhibition Name'),max_length = 255)
    slug = models.SlugField(_('Slug'))
    organizer = models.CharField(_('Organizer'),max_length = 255)
    location = models.ForeignKey(Locations)                             #Foreign Key with Locations
    city = models.CharField(_('City'),max_length = 255)
    address = models.CharField(_('Address'),max_length = 255)
    workmode = models.CharField(_('Work Mode'),max_length = 200)
    startdate = models.DateField(_('Start Date'))
    enddate = models.DateField(_('End Date'))
    date = models.DateField(_('Date'))
    building = models.CharField(_('Building'),max_length = 255)
    photo = models.ImageField(_('Photo'),upload_to = 'exhibitions')
    fullname = models.CharField(_('Fullname'),max_length = 255)
    phone = models.CharField(_('Phone'),max_length = 255)
    fax = models.CharField(_('Fax'),max_length = 255,null = True)
    email = models.EmailField(_('Email'))
    site = models.URLField(_('Site'),null = True)
    description = models.TextField(_('Description'))

    class Meta:
        verbose_name = _('Exhibitions')
        verbose_name_plural = _('Exhibitions')


