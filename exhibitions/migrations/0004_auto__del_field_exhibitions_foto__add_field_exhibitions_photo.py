# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Exhibitions.foto'
        db.delete_column(u'exhibitions_exhibitions', 'foto')

        # Adding field 'Exhibitions.photo'
        db.add_column(u'exhibitions_exhibitions', 'photo',
                      self.gf('django.db.models.fields.files.ImageField')(default=2, max_length=100),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Exhibitions.foto'
        db.add_column(u'exhibitions_exhibitions', 'foto',
                      self.gf('django.db.models.fields.files.ImageField')(default=2, max_length=100),
                      keep_default=False)

        # Deleting field 'Exhibitions.photo'
        db.delete_column(u'exhibitions_exhibitions', 'photo')


    models = {
        u'core.locations': {
            'Meta': {'object_name': 'Locations'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'exhibitions.exhibitions': {
            'Meta': {'object_name': 'Exhibitions'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'building': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'enddate': ('django.db.models.fields.DateField', [], {}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'fullname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Locations']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'organizer': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'startdate': ('django.db.models.fields.DateField', [], {}),
            'workmode': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['exhibitions']