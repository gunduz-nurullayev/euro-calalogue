from django.conf.urls import patterns, url
from exhibitions import views
# from workspace import views as wp_views


urlpatterns = patterns('',
    url(r'^(?P<slug>[-\w]+)/$',views.exhibition_detail, name='exhibition_detail'),
    url(r'^$', views.index,  name='exhibitions_archive_index'),
    )
