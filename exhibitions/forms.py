# from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _

from exhibitions.models import Exhibitions
from django.contrib.auth import authenticate, login, logout
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.forms import ModelForm


class ExhibitionsForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Exhibitions
        fields = '__all__'