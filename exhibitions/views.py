from django.shortcuts import render
# Http
from django.http import HttpResponse, Http404,HttpResponseRedirect,HttpRequest
# Template
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from exhibitions.models import Exhibitions
from core.models import Company
from django.http import HttpResponse
from django.views.generic import ListView
from django.utils import translation
from django.db.models import Q
from core.models import Locations
from core.functions import paginate, get_pages
from news.settings import ARCHIVE_PAGE_SIZE

# Create your views here.

# home page
def index(request):

    exhibitions = Exhibitions.objects.all()
    pages = get_pages(request, exhibitions, ARCHIVE_PAGE_SIZE)
    #
    page_count = len(pages.paginator.page_range)
    current_page = pages.number
    page_list = paginate(ARCHIVE_PAGE_SIZE,page_count,current_page)
    dict = {'pages': pages, 'page_list': page_list, 'page_count': page_count}

    return render_to_response('exhibitions/all_exhibitions.html',dict,context_instance=RequestContext(request))



def exhibition_detail(request, slug):
    exhibition = Exhibitions.objects.get(slug=slug)
    return render_to_response( 'exhibitions/exhibition_detail.html', {'object':exhibition},context_instance=RequestContext(request))

