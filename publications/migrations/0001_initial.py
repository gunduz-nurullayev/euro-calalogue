# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Publications'
        db.create_table(u'publications_publications', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Locations'])),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('source', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
            ('author', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('viewcount', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('senddate', self.gf('django.db.models.fields.DateField')()),
            ('updatedate', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'publications', ['Publications'])

        # Adding model 'PublicationLinks'
        db.create_table(u'publications_publicationlinks', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'publications', ['PublicationLinks'])

        # Adding M2M table for field category on 'PublicationLinks'
        m2m_table_name = db.shorten_name(u'publications_publicationlinks_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('publicationlinks', models.ForeignKey(orm[u'publications.publicationlinks'], null=False)),
            ('categories', models.ForeignKey(orm[u'catalog.categories'], null=False))
        ))
        db.create_unique(m2m_table_name, ['publicationlinks_id', 'categories_id'])


    def backwards(self, orm):
        # Deleting model 'Publications'
        db.delete_table(u'publications_publications')

        # Deleting model 'PublicationLinks'
        db.delete_table(u'publications_publicationlinks')

        # Removing M2M table for field category on 'PublicationLinks'
        db.delete_table(db.shorten_name(u'publications_publicationlinks_category'))


    models = {
        u'catalog.categories': {
            'Meta': {'object_name': 'Categories'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'meta_keyword': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'page_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Categories']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'core.locations': {
            'Meta': {'object_name': 'Locations'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'publications.publicationlinks': {
            'Meta': {'object_name': 'PublicationLinks'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['catalog.Categories']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'publications.publications': {
            'Meta': {'object_name': 'Publications'},
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Locations']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'senddate': ('django.db.models.fields.DateField', [], {}),
            'source': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updatedate': ('django.db.models.fields.DateField', [], {}),
            'viewcount': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        }
    }

    complete_apps = ['publications']