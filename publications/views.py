
from . import models
from publications.models import Publications
from news.settings import ARCHIVE_PAGE_SIZE
from core.functions import paginate, get_pages
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.views.generic import ListView
from django.utils import translation
from catalog.models import Categories

def publications_list(request):
    items = models.Publications.objects.all()
    pages = get_pages(request, items, ARCHIVE_PAGE_SIZE)
    #
    page_count = len(pages.paginator.page_range)
    current_page = pages.number
    page_list = paginate(ARCHIVE_PAGE_SIZE,page_count,current_page)
    dict = {'pages': pages, 'page_list': page_list, 'page_count': page_count}
    return render_to_response('publications/all_publications.html', dict ,context_instance=RequestContext(request))



def publications_detail(request, slug):
    publication = Publications.objects.get(slug=slug)
    return render_to_response( 'publications/publications_detail.html', {'object':publication},context_instance=RequestContext(request))



class CategoryPublicationsView(ListView):
    model = Publications
    template_name = 'publications/category_publications.html'
    def get_context_data(self, *args, **kwargs):
        context = super(CategoryPublicationsView, self).get_context_data(**kwargs)
        context['slug'] = Publications.objects.all()
        category_obj = Categories.objects.get(slug=self.kwargs['slug'])

        category_ob = Publications.objects.filter(category=category_obj.id)

        cur_language = translation.get_language()
        context['cur_language'] = cur_language
        context['publications'] = category_ob
        context['category'] = category_obj

    # context['vacancy_list']=Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        return context

# def category_publications(self,request,**kwargs):
#     publications = Publications.objects.filter(category = self.kwargs['slug'])
#     return  render_to_response('publications/category_publications.html',{'publications':publications},context_instance=RequestContext(request))