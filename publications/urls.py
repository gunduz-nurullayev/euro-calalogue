# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from publications import views





urlpatterns = patterns('',
    url(r'^$',views.publications_list, name='publications_archive_index'),
    url(r'^c-(?P<slug>.*?)/$',views.CategoryPublicationsView.as_view(), name='category_publications'),

    url(r'^(?P<slug>[-\w]+)/$',views.publications_detail, name='publication_detail'),
     # url(r'^(?P<slug>.*?)-category/$',views.CategoryPublicationsView.as_view(), name='category_publications'),

)
