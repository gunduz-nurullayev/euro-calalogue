from django.db import models
from django.utils.translation import ugettext as _
from core.models import Locations,Categories
from news import settings
from django.core.urlresolvers import reverse

# Create your models here.

class Publications(models.Model):
    title = models.CharField(_('Title'),max_length = 255)
    slug = models.SlugField(_('Slug'),help_text=_('A slug is a short name which uniquely identifies the publications item for this day'))
    description = models.TextField(_('Description'))
    location = models.ForeignKey(Locations)                         #Foreign Key with Locations
    text = models.TextField(_('Text'))
    source = models.URLField(_('Source'),null = True)
    author = models.CharField(_('Author'),max_length = 255,null = True)
    photo = models.ImageField(_('Photo'),upload_to = 'publications')
    viewcount = models.CharField(_('View Count'),max_length = 100,null = True)
    senddate = models.DateField(_('Send Date'))
    updatedate = models.DateField(_('Update Date'))
    category = models.ManyToManyField(Categories,'Category')

    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name = _('Publications')
        verbose_name_plural = _('Publications')
    # def get_absolute_url(self):
    #     if settings.LINK_AS_ABSOLUTE_URL and self.link:
    #         if settings.USE_LINK_ON_EMPTY_CONTENT_ONLY and not self.content:
    #             return self.link
    #     return reverse('publications:publication_detail', kwargs={'slug': self.slug})
    #



# class PublicationLinks(models.Model):
#     category = models.ForeignKey(Categories)
#     publication = models.ForeignKey(Publications)

