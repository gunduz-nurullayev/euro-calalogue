from django.contrib import admin
from publications.models import Publications
from publications.forms import PublicationsForm
# Register your models here.

class PublicationsAdmin(admin.ModelAdmin):

    form = PublicationsForm
    prepopulated_fields = {'slug': ('title',),}

    list_display = ('title','author','location')

admin.site.register(Publications,PublicationsAdmin)

# class PublicationLinksAdmin(admin.ModelAdmin):
#
#     form = PublicationLinksForm
#
#     # list_display = ('category','publication')
# admin.site.register(PublicationLinks,PublicationLinksAdmin)