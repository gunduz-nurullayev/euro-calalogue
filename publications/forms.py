from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login, logout
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from django.forms import ModelForm
from publications.models import Publications



class PublicationsForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Publications
        fields = '__all__'

# class PublicationLinksForm(forms.ModelForm):
#
#     class Meta:
#         model = PublicationLinks
