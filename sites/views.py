from . import models
from sites.models import Sites
from news.settings import ARCHIVE_PAGE_SIZE
from core.functions import paginate, get_pages
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.views.generic import ListView
from django.utils import translation
from catalog.models import Categories

def sites_list(request):
    items = models.Sites.objects.all()
    pages = get_pages(request, items, ARCHIVE_PAGE_SIZE)
    #
    page_count = len(pages.paginator.page_range)
    current_page = pages.number
    page_list = paginate(ARCHIVE_PAGE_SIZE,page_count,current_page)
    dict = {'pages': pages, 'page_list': page_list, 'page_count': page_count}
    return render_to_response('sites/all_sites.html', dict ,context_instance=RequestContext(request))

