from django.contrib import admin
from django.utils.translation import ugettext as _
from sites.models import Sites
from sites.forms import SitesForm
# Register your models here.

class SitesAdmin(admin.ModelAdmin):

    form = SitesForm

    #     # date_hierarchy = 'pub_date'
    list_display = ('title', 'location','clicks')
#     #list_editable = ('title', 'is_published')
#     # list_filter = ('is_published', )
#     # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('title',)}
#     # form = LibraryForm

admin.site.register(Sites,SitesAdmin)
