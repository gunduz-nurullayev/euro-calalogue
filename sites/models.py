from django.db import models
from django.utils.translation import ugettext as _
from core.models import Locations,Categories
from django.core.validators import MaxLengthValidator

# Create your models here.


class Sites(models.Model):
    title = models.CharField(_('Title'),max_length = 255)
    slug = models.SlugField(_('Slug'),help_text=_('A slug is a short name which uniquely identifies the sites item for this day'))

    description = models.TextField(_('Description'),validators=[MaxLengthValidator(500)])
    link = models.URLField(_('Link'))
    location = models.ForeignKey(Locations)
    senddate = models.DateField(_('Send Date'))
    logo = models.ImageField(_('Logo'),upload_to = 'sites')
    active = models.CharField(_('Active'),max_length = 255)
    clicks = models.CharField(_('Clicks'),max_length = 255)
    category = models.ManyToManyField(Categories,'Category')

    class Meta:
        verbose_name = _('Sites')
        verbose_name_plural = _('Sites')


    def __unicode__(self):
        return self.title
