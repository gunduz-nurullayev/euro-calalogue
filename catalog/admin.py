from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from catalog.models import Categories
from catalog.forms import CategoriesForm

# Register your models here.


class CategoriesAdmin(admin.ModelAdmin):
    form = CategoriesForm

    #     # date_hierarchy = 'pub_date'
    list_display = ('title','parent','slug', 'page_title','status')
#     #list_editable = ('title', 'is_published')
#     # list_filter = ('is_published', )
#     # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('title',)}
#     # form = LibraryForm

admin.site.register(Categories,CategoriesAdmin)
