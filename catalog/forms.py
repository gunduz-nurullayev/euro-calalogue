# from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _

from catalog.models import Categories
from django.contrib.auth import authenticate, login, logout
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.forms import ModelForm


class CategoriesForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())
    icon = forms.ImageField(required =False)
    class Meta:
        model = Categories
        fields = '__all__'