from modeltranslation.translator import translator, TranslationOptions
from ckeditor.widgets import CKEditorWidget
from catalog.models import Categories
from django import forms


class VacancyTranslationOptions(TranslationOptions):
    fields = ('title','page_title','description','meta_description','meta_keywords')

# class PageTranslationOptions(TranslationOptions):
#     fields = ('title','slug','content')
#     class Meta:
#         model = Page
#         widgets = {
#            'content' : forms.Textarea(attrs={'class':'ckeditor'}),
    #        # 'field_2' : forms.Textarea(attrs={'class':'ckeditor'}),
    #     }


translator.register(Vacancy,VacancyTranslationOptions)
# translator.register(Page,PageTranslationOptions)
