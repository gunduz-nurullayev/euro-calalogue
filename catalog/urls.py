from django.conf.urls import patterns, url
from catalog import views
# from workspace import views as wp_views


urlpatterns = patterns('',
    url(r'^dialog/(?P<id>.*?)/$', views.SomeCategoryView.as_view(),name='popup'),
    url(r'^search/$', views.popup,name='popcomp'),
    # url(r'^(?P<category>.*?)/(?P<slugk>.*?)/(?P<slugl>.*?)/(\w+)/$', views.CompanyFilterView.as_view(),  name='filter'),
    # url(r'^(?P<category>.*?)/(?P<slugk>.*?)/(?P<slugl>.*?)/(\w+)/$', views.LastCategoryView.as_view(),  name='lastcategory'),
    url(r'^(?P<category>.*?)/(?P<slugk>.*?)/(?P<slugl>.*?)/$', views.LastCategoryView.as_view(),  name='lastcategory'),

    url(r'^(?P<category>.*?)/(?P<slug>.*?)/$', views.DownCategoryView.as_view(),  name='downcategory'),

    url(r'^(?P<slug>.*?)/$', views.CategoryView.as_view(),  name='category'),
    url(r'^$', views.index,  name='index'),

    #user actions

)
