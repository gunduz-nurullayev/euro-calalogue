# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Categories.description'
        db.alter_column(u'catalog_categories', 'description', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Categories.level'
        db.alter_column(u'catalog_categories', 'level', self.gf('django.db.models.fields.PositiveIntegerField')(null=True))

        # Changing field 'Categories.slug'
        db.alter_column(u'catalog_categories', 'slug', self.gf('django.db.models.fields.SlugField')(max_length=255))

        # Changing field 'Categories.icon'
        db.alter_column(u'catalog_categories', 'icon', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True))

    def backwards(self, orm):

        # Changing field 'Categories.description'
        db.alter_column(u'catalog_categories', 'description', self.gf('django.db.models.fields.TextField')(default=2))

        # Changing field 'Categories.level'
        db.alter_column(u'catalog_categories', 'level', self.gf('django.db.models.fields.PositiveIntegerField')(default=2))

        # Changing field 'Categories.slug'
        db.alter_column(u'catalog_categories', 'slug', self.gf('django.db.models.fields.SlugField')(max_length=50))

        # Changing field 'Categories.icon'
        db.alter_column(u'catalog_categories', 'icon', self.gf('django.db.models.fields.files.ImageField')(default=2, max_length=100))

    models = {
        u'catalog.categories': {
            'Meta': {'object_name': 'Categories'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'meta_keyword': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'page_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Categories']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['catalog']