# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Categories.meta_description'
        db.alter_column(u'catalog_categories', 'meta_description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Categories.meta_keyword'
        db.alter_column(u'catalog_categories', 'meta_keyword', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):

        # Changing field 'Categories.meta_description'
        db.alter_column(u'catalog_categories', 'meta_description', self.gf('django.db.models.fields.CharField')(default=2, max_length=255))

        # Changing field 'Categories.meta_keyword'
        db.alter_column(u'catalog_categories', 'meta_keyword', self.gf('django.db.models.fields.CharField')(default=2, max_length=255))

    models = {
        u'catalog.categories': {
            'Meta': {'object_name': 'Categories'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'meta_keyword': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'page_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Categories']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['catalog']