# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Categories.parent'
        db.add_column(u'catalog_categories', 'parent',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Categories'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Categories.parent'
        db.delete_column(u'catalog_categories', 'parent_id')


    models = {
        u'catalog.categories': {
            'Meta': {'object_name': 'Categories'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'meta_keyword': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'page_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Categories']", 'null': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['catalog']