from django.shortcuts import render
# Http
from django.http import HttpResponse, Http404,HttpResponseRedirect,HttpRequest
# Template
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from catalog.models import Categories
from core.models import Company
from django.http import HttpResponse
from django.views.generic import ListView
from django.utils import translation
from django.db.models import Q
from core.models import Locations
# Create your views here.

# home page
def index(request):

    categories = Categories.objects.all()
    return render_to_response('catalog/all_categories.html',{'categories':categories},context_instance=RequestContext(request))

class CategoryView(ListView):
    model = Categories
    template_name = 'catalog/parent_category.html'
    def get_context_data(self, *args, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['slug'] = Categories.objects.all()
        category_obj = Categories.objects.get(slug=self.kwargs['slug'])

        category_ob = Categories.objects.filter(parent_id=category_obj.id)

        cur_language = translation.get_language()
        context['cur_language'] = cur_language
        context['categories'] = category_ob
        context['title'] = category_obj.title

    # context['vacancy_list']=Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        return context

class LastCategoryView(ListView):
    model = Categories
    template_name = 'catalog/last_category.html'
    def get_context_data(self, *args, **kwargs):
        context = super(LastCategoryView, self).get_context_data(**kwargs)
        location = Locations.objects.all()
        # subj_terms = Term.objects.filter(subject = subject_obj.id)
        GLOSSARY_FILTERS = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

        glossaries = {}
        glossaries1 = {}


        # if subj_terms:
        #     for letter in GLOSSARY_FILTERS:
        #         term_list =[]
        #         for term in subj_terms:
        #             if  term.title.upper().startswith(letter):
        #                 term_list.append(term.title)
        #                 term_list.append(term.description)
        #
        #                 glossaries.update({letter:term_list})
        # glossaries = OrderedDict(sorted(glossaries.items(), key=lambda t: t[0]))
        # url = self.request.build_absolute_uri
        url = self.request.META['QUERY_STRING']


        context['glossary'] =GLOSSARY_FILTERS
        context['slugl'] = Categories.objects.all()
        category_obj = Categories.objects.get(slug=self.kwargs['slugl'])
        upcategory = Categories.objects.get(slug=self.kwargs['category'])
        downcategory = Categories.objects.get(slug=self.kwargs['slugk'])

        # if not self.request.META['HTTP_REFERER']:

        company = Company.objects.filter(category = category_obj.id)

        l = ''
        s = ''
        type = ''
        sorturl = ''
        typeurl = ''
        countryurl = ''
        url = '?type=&country=&sort=&l='
        if '&type' in self.request.META['QUERY_STRING']:
            type = self.request.GET['type']

        # typeurl = Replace(self.request.META['QUERY_STRING'],)
        if "c" in self.request.GET:
            countryurl = self.request.META['QUERY_STRING'].replace('c='+self.request.GET['c'],'')

            c = self.request.GET['c']
            if c == 'all':
                company = company
            else:
                country = Locations.objects.get(code2 = c)
                company = company.filter(location = country.id)

        if "l" in self.request.GET:
            l = self.request.GET['l']
            if l == '0':
                company = company.filter(Q(name__istartswith=l))
            else:
                company = company.filter(name__istartswith=l)

        # typeurl = self.request.META['QUERY_STRING'].replace('type='+self.request.GET['type'],'')

        if "type" in self.request.GET:
            # typeurl = self.request.META['QUERY_STRING'].replace(self.request.META['QUERY_STRING'],'type='+self.request.GET['type'])

            type = self.request.GET['type']
            if type == '0':
                company =company
            else:
                company = company.filter(comp_type = self.request.GET['type'])

        if "s" in self.request.GET:
            sorturl = self.request.META['QUERY_STRING'].replace('&s='+self.request.GET['s'],'')

            s = self.request.GET['s']
            if s == '1':
                company = company.order_by('name')
            if s == '2':
                company = company.order_by('-date')
            if s == '3':
                company = company.order_by('-clicks')


        context['upcategory'] = upcategory
        context['downcategory'] = downcategory
        context['lastcategory'] = category_obj
        context['locations'] = location
        context['url'] = url
        context['typeurl'] = typeurl
        context['countryurl'] = countryurl
        context['sorturl'] = sorturl
        context['l'] = l
        context['type'] = type
        context['s'] = s
        context['companies'] = company
        cur_language = translation.get_language()
        context['cur_language'] = cur_language

    # context['vacancy_list'] = Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        return context

class DownCategoryView(ListView):
    model = Categories
    template_name = 'catalog/second_category.html'
    def get_context_data(self, *args, **kwargs):
        context = super(DownCategoryView, self).get_context_data(**kwargs)
        category_obj = Categories.objects.get(slug=self.kwargs['slug'])

        category_ob = Categories.objects.filter(parent_id=category_obj.id)
        context['categories'] = category_ob
        cur_language = translation.get_language()
        context['cur_language'] = cur_language
        context['title'] = category_obj.title
    # context['vacancy_list']=Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        return context

        # return self.render_to_response(context)
class CompanyFilterView(ListView):
    model = Company
    template_name = 'catalog/second_category.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CompanyFilterView, self).get_context_data(**kwargs)
        a = LastCategoryView.context_object_name.company.order_by('name')
        context['a'] = a

        return context

# def get_date(request):
#
#     qs = Company.MyModel.objects.all()
#
#     sort = request.session.get('TALENT_SORT', None)
#     if sort in ["alphabetical", "created_date"]:
#         qs = qs.order_by(sort)
#
#     myfilter = request.session.get("TALENT_FILTER", None)
#     if myfilter in ["top_ten","bottom_ten"]:
#         qs = qs.filter( .... )
#     return render_to_response()



class SomeCategoryView(ListView):
    model = Categories
    template_name = 'catalog/popup.html'
    def get_context_data(self, *args, **kwargs):
        # if self.request.is_ajax():
        context = super(SomeCategoryView, self).get_context_data(**kwargs)
        context['id'] = Categories.objects.all()
        cat2 = Categories.objects.get(id=self.kwargs['id'])
        cat1 = Categories.objects.get(id=cat2.parent_id)

        category_obj = Categories.objects.filter(parent_id=self.kwargs['id'])

        # company = Company.objects.filter(category = category_obj.id)
        context['categories'] = category_obj
        context['cat2'] = cat2.slug
        context['cat1'] = cat1.slug


        cur_language = translation.get_language()
        context['cur_language'] = cur_language

    # context['vacancy_list']=Vacancy.objects.filter(~Q(slug=self.kwargs['slug']))
        return context

def popup(request):
    # categories=[]
    company = []

    # if request.method = "POST":
    categories = request.POST.getlist('categories')
    # categories = request.POST["categories"].split("_")

    company = Company.objects.filter(category__in = categories).distinct()
    # company = Company.objects.filter(category__in=categories).distinct(id)

    # categories = Categories.objects.filter(parent_id =id)
    return render_to_response('company/filtir_company.html',{'categories':categories,'company':company},context_instance=RequestContext(request))


