from django.db import models
from django.utils.translation import ugettext as _

# Create your models here

class Categories(models.Model):
    parent = models.ForeignKey('self',blank=True,null=True)
    title = models.CharField(_('Title'),max_length = 255)
    slug = models.SlugField(_('Slug'),max_length=255)
    page_title = models.CharField(_('Page Title'),max_length = 255)
    icon = models.ImageField(_('Icon'),upload_to = 'catalog/categories',null=True,blank=True)
    description = models.TextField(_('Description'),null=True,blank=True)
    status = models.CharField(_('Status'),max_length = 255)
    meta_description = models.CharField(_('Meta Description'),max_length = 255,null=True,blank=True)
    meta_keyword = models.CharField(_('Meta Keyword'),max_length = 255,null=True,blank=True)
    level = models.PositiveIntegerField(_('Category Level'),null=True,blank=True)
    def __unicode__(self):
        return self.title

    class Meta:
         verbose_name = _('Category')
         verbose_name_plural = _('Categories')


