from django.views import generic as generic_views
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from . import models
from models import News
from settings import ARCHIVE_PAGE_SIZE
from core.functions import paginate, get_pages
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext


def news_list(request):
    items = models.News.published.all()
    pages = get_pages(request, items, ARCHIVE_PAGE_SIZE)

    page_count = len(pages.paginator.page_range)
    current_page = pages.number
    page_list = paginate(ARCHIVE_PAGE_SIZE,page_count,current_page)
    dict = {'pages': pages, 'page_list': page_list, 'page_count': page_count}
    return render_to_response('news/all_news.html', dict,context_instance=RequestContext(request))





def news_detail(request, slug):
    news = News.objects.get(slug=slug)
    return render_to_response( 'news/news_detail.html', {'object':news},context_instance=RequestContext(request))
