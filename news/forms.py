from __future__ import absolute_import
from django import forms
from ckeditor.widgets import CKEditorWidget

from news.models import News


class NewsForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = News
        fields = '__all__'

    # def __init__(self, *args, **kwargs):
    #     super(NewsForm, self).__init__(*args, **kwargs)
    #
    #     self.fields['content'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
    #     self.fields['content_en'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
    #     self.fields['content_ru'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
    #     self.fields['content_tr'].widget = CKEditorWidget(attrs={'cols': 100, 'rows': 20, 'style':'resize:none;'})
#