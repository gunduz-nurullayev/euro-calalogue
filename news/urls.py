# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from news import views





urlpatterns = patterns('',
    url(r'^$',
        views.news_list, name='news_archive_index'),

    url(r'^(?P<slug>[-\w]+)/$',
        views.news_detail, name='news_detail'),
)
