# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'News'
        db.delete_table(u'news_news')


    def backwards(self, orm):
        # Adding model 'News'
        db.create_table(u'news_news', (
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('source', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Locations'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('update_date', self.gf('django.db.models.fields.DateField')()),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'news', ['News'])


    models = {
        
    }

    complete_apps = ['news']