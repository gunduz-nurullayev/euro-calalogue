# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Library.description'
        db.alter_column(u'library_library', 'description', self.gf('django.db.models.fields.TextField')())

    def backwards(self, orm):

        # Changing field 'Library.description'
        db.alter_column(u'library_library', 'description', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'library.library': {
            'Meta': {'object_name': 'Library'},
            'active': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'bookcover': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'bookurl': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'page_count': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'pubhouse': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'senddate': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updatedate': ('django.db.models.fields.DateField', [], {'null': 'True'})
        }
    }

    complete_apps = ['library']