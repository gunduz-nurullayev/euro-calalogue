from django.shortcuts import render
# Http
from django.http import HttpResponse, Http404,HttpResponseRedirect,HttpRequest
# Template
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from library.models import Library
from core.models import Company
from django.http import HttpResponse
from django.views.generic import ListView
from django.utils import translation
from django.db.models import Q
from core.models import Locations
from core.functions import paginate, get_pages
from news.settings import ARCHIVE_PAGE_SIZE
from business_info.models import Stage,BusinessInfo
from django.contrib.contenttypes.models import ContentType
# Create your views here.

# home page
class LibraryView(ListView):
    template_name = 'library/book_list.html'
    model = Library
    def get_context_data(self, **kwargs):
        context = super(LibraryView, self).get_context_data(**kwargs)
        context['slug'] = Stage.objects.all()
        category_obj = Stage.objects.get(slug=self.kwargs['slug'])
        # objectss = BusinessInfo.objects.filter(stage = category_obj.id)
        ctype = ContentType.objects.get(model='library')
        books =BusinessInfo.objects.filter(Q(content_link = ctype) & Q(stage = category_obj.id))

        books_list =[]

        for y in books:
            books_list.append(y.object_id)

        book = Library.objects.filter(id__in= books_list)
        context['stage'] = category_obj
        context['books'] = book
        return context

class BookDetailView(ListView):
    template_name = 'library/book_detail.html'
    model = Library
    def get_context_data(self, **kwargs):
        context = super(BookDetailView, self).get_context_data(**kwargs)
        context['slug'] = Library.objects.all()
        book_obj = Library.objects.get(slug=self.kwargs['slug'])
        # objectss = BusinessInfo.objects.filter(stage = category_obj.id)



        context['book'] = book_obj
        return context
#
# def index(request):
#
#     exhibitions = Library.objects.all()
#     pages = get_pages(request, exhibitions, ARCHIVE_PAGE_SIZE)
#     #
#     page_count = len(pages.paginator.page_range)
#     current_page = pages.number
#     page_list = paginate(ARCHIVE_PAGE_SIZE,page_count,current_page)
#     dict = {'pages': pages, 'page_list': page_list, 'page_count': page_count}
#
#     return render_to_response('exhibitions/all_exhibitions.html',dict,context_instance=RequestContext(request))



# def exhibition_detail(request, slug):
#     exhibition = Library.objects.get(slug=slug)
#     return render_to_response( 'exhibitions/exhibition_detail.html', {'object':exhibition},context_instance=RequestContext(request))

