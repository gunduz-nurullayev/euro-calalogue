from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from library.models import Library
from library.forms import LibraryForm

# Register your models here.


class LibraryAdmin(admin.ModelAdmin):
    form = LibraryForm

    #     # date_hierarchy = 'pub_date'
    list_display = ('title', 'author','page_count')
#     #list_editable = ('title', 'is_published')
#     # list_filter = ('is_published', )
#     # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('title',)}
#     # form = LibraryForm

admin.site.register(Library,LibraryAdmin)
