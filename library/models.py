from django.db import models
from django.utils.translation import ugettext as _



class Library(models.Model):
    title = models.CharField(_('Title'),max_length = 255)
    slug = models.SlugField(_('Slug'))
    description = models.TextField(_('Description'))
    author = models.CharField(_('Author'),max_length = 200)
    page_count = models.CharField(_('Page Count'),max_length = 50)
    bookurl = models.URLField(_('Book URL'),null=True)
    bookcover = models.CharField(_('Book Cover'),max_length = 50,null=True)
    pubhouse = models.CharField(_('Published House'),max_length = 255,null=True)
    image = models.ImageField(_('Image'),upload_to = 'library')
    active = models.CharField(_('Active'),max_length = 255,null=True)
    senddate = models.DateField(_('Send Date'),null=True)
    updatedate = models.DateField(_('Update Date'),null=True)
